﻿#####
## Azure Storage Cleanup
#####

# Source account.
$SrcStorageAccount="patientlife"
$SrcKey = "eJ5VJ6Rx1y7MsE0xYlSSl8a5qBwOZvDIHzI/ZORnaLzT0g9KFcpV+/ZltSDRGCy25BJdFWRhy6nihWvr6yB7pQ==";
 
# Context used to retrieve containers.
$SrcContext = New-AzureStorageContext -StorageAccountName $SrcStorageAccount -StorageAccountKey $SrcKey
 
# Get a list of containers at the source.
$SrcContainers = Get-AzureStorageContainer -Context $SrcContext

Write-Output "Cleaning up.."

# Iterate containers
ForEach ($srcContainer in $SrcContainers) {
  $containerName = $srcContainer.Name
  Remove-AzureStorageContainer -Name $containerName -Context $SrcContext -Force
}