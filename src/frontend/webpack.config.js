const R = require('ramda');
const path = require('path');
const webpack = require('webpack');

// For reasons, the destructuring syntax isn't working in webpack.
const filter = R.filter;
const identity = R.identity;
const any = R.any;

const hasProductionFlag = any(x => x === '-p');

// Removes null elements from an array.
const clean = filter(identity);

// Are we in production or development?
const prod = process.env.NODE_ENV === 'production' || hasProductionFlag(process.argv);
const dev = !prod;

module.exports = {
  devtool: dev ? 'cheap-module-eval-source-map' : 'source-map',
  debug: dev,
  entry: clean([
    'babel-polyfill',
    dev ? 'webpack-hot-middleware/client' : null,
    './src/frontend/app/index'
  ]),
  output: {
    // Emit files in the backend folder, because that's where
    // it'll be served from in production.
    path: path.join(__dirname, '/../backend/output'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  plugins: clean([
    new webpack.optimize.OccurenceOrderPlugin(),
    dev ? new webpack.HotModuleReplacementPlugin() : null,
    prod ? new webpack.optimize.UglifyJsPlugin({
      /*eslint-disable */
      output: {
        comments: false
      },
      mangle: true,
      compress: {
        sequences: true,
        dead_code: true,
        conditionals: true,
        booleans: true,
        unused: true,
        if_return: true,
        join_vars: true,
        drop_console: true
      }
      /*eslint-enable */
    }) : null,
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(prod ? 'production' : 'development')
    }),
    new webpack.NoErrorsPlugin()
  ]),
  module: {
    loaders: [{
      test: /\.jsx?$/,
      loaders: [ 'babel' ],
      exclude: /node_modules/,
      include: __dirname
    }, {
      test: /\.s?css$/,
      loaders: [
        'style',
        'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]',
        'sass'
      ]
    }]
  },
  resolve: {
    modulesDirectories: ['./app', 'node_modules']
  }
};