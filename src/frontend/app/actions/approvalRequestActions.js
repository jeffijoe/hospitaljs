import http from 'util/http';
import { showNotification } from './notificationActions';
export const FETCH_APPROVAL_REQUESTS = 'approvalRequests.FETCH_APPROVAL_REQUESTS';
export const FETCH_APPROVAL_REQUESTS_RESULT = 'approvalRequests.FETCH_APPROVAL_REQUESTS_RESULT';

export const fetchApprovalRequests = () => async (dispatch) => {
  dispatch({
    type: FETCH_APPROVAL_REQUESTS
  });

  const url = `/api/approvalrequests`;
  const response = await http.get(url);
  let success, approvalRequests;
  if (response.status > 299) {
    success = false;
  } else {
    success = true;
    approvalRequests = response.data;
  }

  dispatch({
    type: FETCH_APPROVAL_REQUESTS_RESULT,
    approvalRequests,
    success
  });
};

export const SET_VISIBILITY_FILTER = 'approvalRequests.SET_VISIBILITY_FILTER';

export const VisibilityFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_REJECTED: 'SHOW_REJECTED',
  SHOW_APPROVED: 'SHOW_APPROVED',
  SHOW_PENDING: 'SHOW_PENDING'
};

const _statusMap = {
  'pending': VisibilityFilters.SHOW_PENDING,
  'rejected': VisibilityFilters.SHOW_REJECTED,
  'approved': VisibilityFilters.SHOW_APPROVED
};

VisibilityFilters.fromStatus = (status) => _statusMap[status] || VisibilityFilters.ALL;

export function setVisibilityFilter (filter) {
  return { type: SET_VISIBILITY_FILTER, filter };
}

const successMessages = {
  'approved': 'Record approved!',
  'rejected': 'Record rejected!'
};

export const UPDATE_APPROVAL_REQUEST = 'approvalRequests.UPDATE_APPROVAL_REQUEST';
export const UPDATE_APPROVAL_REQUEST_RESULT = 'approvalRequests.UPDATE_APPROVAL_REQUEST_RESULT';
export const updateApprovalRequest = (id, status, rejectedReason) => async (dispatch, getState) => {
  dispatch({
    type: UPDATE_APPROVAL_REQUEST,
    id,
    status
  });

  const url = `/api/approvalrequests/${id}`;
  const response = await http.put(url, {
    status,
    ...status === 'rejected' && {
      rejectedReason
    }
  });

  let approvalRequest, success = false;
  if (response.status < 300) {
    success = true;
    approvalRequest = response.data;
  }

  dispatch({
    type: UPDATE_APPROVAL_REQUEST_RESULT,
    id,
    success,
    approvalRequest
  });

  if (success) {
    const msg = successMessages[status] || 'Status changed!';
    dispatch(showNotification(msg));
  }
};


export const TOGGLE_REJECTION_DIALOG = 'approvalRequests.TOGGLE_REJECTION_DIALOG';
export const toggleRejectionDialog = (show, id) => (dispatch, getState) => {
  dispatch({
    type: TOGGLE_REJECTION_DIALOG,
    show,
    requestToRejectId: id
  });
};

export const showRejectionDialog = (id) => toggleRejectionDialog(true, id);
export const cancelRejectionDialog = () => toggleRejectionDialog(false, null);

export const CHANGE_REJECTION_MESSAGE = 'approvalRequests.CHANGE_REJECTION_MESSAGE';
export const changeRejectionMessage = (rejectionMessage) => (dispatch, getState) => {
  dispatch({
    type: CHANGE_REJECTION_MESSAGE,
    rejectionMessage
  });
};

export const approveApprovalRequest = (id) => (dispatch, getState) => {
  dispatch(updateApprovalRequest(id, 'approved'));
};

export const REJECTION_VALIDATION_ERROR = 'approvalrequests.REJECTION_VALIDATION_ERROR';

export const rejectApprovalRequest = () => (dispatch, getState) => {
  const { rejectionMessage, requestToRejectId } = getState().approvalRequests;
  if (!rejectionMessage) {
    dispatch({ type: REJECTION_VALIDATION_ERROR, errorMessage: 'Rejection message is required.' });
    return;
  }

  dispatch(updateApprovalRequest(requestToRejectId, 'rejected', rejectionMessage));
};
