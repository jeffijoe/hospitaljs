import http from 'util/http';
import { routeActions } from 'redux-simple-router';
import moment from 'moment';
import uploadFile from 'util/uploadFile';
import randomString from 'util/randomString';
import { pick } from 'ramda';
import {setBackButtonAction} from './headerActions';
import { showNotification } from './notificationActions';

/**
 * Fetching all records for a health file
 */

export const FETCH_PATIENT_RECORDS = 'patientRecord.FETCH_PATIENT_RECORDS';
export const FETCH_PATIENT_RECORDS_RESULT = 'patientRecord.FETCH_PATIENT_RECORDS_RESULT';

export const fetchPatientRecords = healthFileId => async (dispatch) => {
  dispatch({
    type: FETCH_PATIENT_RECORDS
  });

  const url = `/api/healthfiles/${healthFileId}/patientrecords`;
  const response = await http.get(url);

  let success, records;
  if (response.status > 299) {
    success = false;
  } else {
    success = true;
    records = response.data;
  }

  dispatch({
    type: FETCH_PATIENT_RECORDS_RESULT,
    records
  });
};


/**
 * Fetching and showing a single record.
 */

export const showPatientRecord = (id, replace) => dispatch => {
  dispatch(routeActions[replace ? 'replace' : 'push'](`/record/${id}`));
};

export const FETCH_PATIENT_RECORD = 'patientRecord.FETCH_PATIENT_RECORD';
export const FETCH_PATIENT_RECORD_RESULT = 'patientRecord.FETCH_PATIENT_RECORD_RESULT';

export const fetchPatientRecord = id => async dispatch => {
  dispatch({
    type: FETCH_PATIENT_RECORD,
    id
  });

  const url = `/api/patientrecords/${id}`;
  const response = await http.get(url);
  let success, record;
  if (response.status < 299) {
    success = true;
    record = response.data;
  } else {
    success = false;
  }

  dispatch(setBackButtonAction(routeActions.push(`/healthfile/${record.healthfileId}`)));
  dispatch({
    type: FETCH_PATIENT_RECORD_RESULT,
    success,
    record
  });
};

/**
 * Creating
 */

export const SHOW_NEW_PATIENT_RECORD = 'patientRecordPage.SHOW_NEW_PATIENT_RECORD';
export const showNewPatientRecord = (healthFileId) => dispatch => {
  dispatch(routeActions.push(`/healthfile/${healthFileId}/new-record`));
};

export const NEW_PATIENT_RECORD = 'patientRecord.NEW_PATIENT_RECORD';
export const newPatientRecord = healthFileId => dispatch => {
  dispatch(setBackButtonAction(routeActions.push(`/healthfile/${healthFileId}`)));
  dispatch({
    type: NEW_PATIENT_RECORD,
    healthFileId
  });
};


export const PATIENT_RECORD_INVALID = 'patientRecord.PATIENT_RECORD_INVALID';
export const CREATE_PATIENT_RECORD = 'patientRecord.CREATE_PATIENT_RECORD';
export const CREATE_PATIENT_RECORD_RESULT = 'patientRecord.CREATE_PATIENT_RECORD_RESULT';
const validate = (dispatch, record) => {
  if (!record.diagnosis) {
    dispatch({
      type: PATIENT_RECORD_INVALID,
      validationErrors: {
        diagnosis: 'Diagnosis is required'
      }
    });
    return false;
  }

  return true;
};

export const createPatientRecord = (healthFileId) => async (dispatch, getState) => {
  const patientRecord = getState().patientRecordPage.record;

  if (!validate(dispatch, patientRecord)) return;

  dispatch({
    type: CREATE_PATIENT_RECORD,
    patientRecord,
    healthFileId
  });

  const url = `/api/healthfiles/${healthFileId}/patientrecords`;
  const response = await http.post(url, {
    ...patientRecord
  });

  let success, newRecord;
  if (response.status > 299) {
    success = false;
  } else {
    success = true;
    newRecord = response.data;
  }

  dispatch({
    type: CREATE_PATIENT_RECORD_RESULT,
    success,
    newRecord
  });

  dispatch(showPatientRecord(newRecord.id, true));
  if (success) {
    dispatch(showNotification('Patient record created!'));
  }
};

/**
 * Start editing a record.
 */

export const EDIT_PATIENT_RECORD = 'patientRecord.EDIT_PATIENT_RECORD';
export const editPatientRecord = () => ({ type: EDIT_PATIENT_RECORD });

/**
 * Saving a record.
 */

export const SAVE_PATIENT_RECORD = 'patientRecord.SAVE_PATIENT_RECORD';
export const SAVE_PATIENT_RECORD_RESULT = 'patientRecord.SAVE_PATIENT_RECORD_RESULT';
export const savePatientRecord = () => async (dispatch, getState) => {
  if (!validate(dispatch, getState().patientRecordPage.record)) return;

  dispatch({
    type: SAVE_PATIENT_RECORD
  });
  const { patientRecordPage } = getState();
  const isNew = patientRecordPage.isNew;
  let { record } = patientRecordPage;
  const url = `/api/patientrecords/${isNew ? '' : patientRecordPage.record.id}`;
  const data = pick(
    [
      'id',
      'diagnosis',
      'symptoms',
      'medication',
      'procedures',
      'media',
      'labResults',
      'bodyParts'
    ],
    record
  );

  const mediaPick = pick(['fileName', 'fileSize', 'mediaType', 'fileUrl']);
  data.media = data.media.map(mediaPick);
  const response = await http[isNew ? 'post' : 'put'](url, data);

  let success;
  if (response.status > 299) {
    success = false;
  } else {
    success = true;
    record = response.data;
  }

  dispatch({
    type: SAVE_PATIENT_RECORD_RESULT,
    record,
    success
  });
  if (success) {
    dispatch(showNotification('Patient record saved!'));
  }
};

/**
 * Cancel editing a record.
 */

export const CANCEL_EDITING = 'patientRecord.CANCEL_EDITING';
export const cancelEditingPatientRecord = () => (dispatch, getState) => {
  const state = getState();
  if (state.patientRecordPage.isNew) {
    dispatch(routeActions.push(`/healthfile/${state.patientRecordPage.healthFileId}`));
  } else {
    dispatch({ type: CANCEL_EDITING });
  }
};

/**
 * Changing a record.
 */

export const CHANGE_PATIENT_RECORD = 'patientRecord.CHANGE_PATIENT_RECORD';
export const changePatientRecord = newData => ({
  type: CHANGE_PATIENT_RECORD,
  newData
});

/**
 * Procedures
 */

export const ADD_PROCEDURE = 'patientRecord.ADD_PROCEDURE';
export const CHANGE_PROCEDURE = 'patientRecord.CHANGE_PROCEDURE';
export const REMOVE_PROCEDURE = 'patientRecord.REMOVE_PROCEDURE';

let _procId = 1;
export const addProcedure = () => dispatch => dispatch({
  type: ADD_PROCEDURE,
  id: _procId++,
  dateTime: moment().toDate()
});
export const changeProcedure = ({ id, step }) => ({
  type: CHANGE_PROCEDURE,
  id,
  step
});
export const removeProcedure = (id) => ({ type: REMOVE_PROCEDURE, id });

/**
 * Media
 */
let _mediaId = 1;
export const UPLOAD_PROGRESS = 'patientRecord.UPLOAD_PROGRESS';
const uploadProgress = (id, total, uploaded) => ({
  type: UPLOAD_PROGRESS,
  id,
  total,
  uploaded
});

export const UPLOAD_COMPLETE = 'patientRecord.UPLOAD_COMPLETE';
const uploadComplete = ({ id, fileName, fileSize, fileType, fileUrl }) => ({
  type: UPLOAD_COMPLETE,
  id,
  fileName,
  fileSize,
  fileType,
  fileUrl
});

export const ADD_MEDIA = 'patientRecord.ADD_MEDIA';
export const addMedia = file => async (dispatch, getState) => {
  const id = _mediaId++;
  dispatch({
    type: ADD_MEDIA,
    id,
    fileName: file.name
  });
  const state = getState();
  const { currentUser } = state;
  const hospitalId = currentUser.hospital.id;
  const token = (await http.get('/api/mediatoken')).data.token;
  console.log('Got me some token yo', token);
  const randomPath = randomString(6);
  const result = await uploadFile({
    file,
    endpoint: `https://patientlife.blob.core.windows.net/${hospitalId}/${randomPath}`,
    token,
    progressCb: ({ total, uploaded}) => {
      dispatch(uploadProgress(id, total, uploaded));
    }
  });

  dispatch(uploadComplete({id, ...result}));
};

export const REMOVE_MEDIA = 'patientRecord.REMOVE_MEDIA';
export const removeMedia = (id) => ({
  type: REMOVE_MEDIA,
  id
});

export const TOGGLE_BODYPART = 'patientRecord.TOGGLE_BODYPART';
export const toggleBodyPart = (bodyPartPath) => ({
  type: TOGGLE_BODYPART,
  bodyPartPath
});