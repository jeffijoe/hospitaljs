import {routeActions} from 'redux-simple-router';

// Action type, used by reducers to calculate the next state.
export const CHANGE_SSN = 'healthFileLookup.CHANGE_SSN';

// The action creator - given an SSN, returns
// an action describing what should happen.
export function changeSsn (ssn) {
  return {
    type: CHANGE_SSN,
    ssn
  };
}

export const openHealthFile = () => (dispatch, getState) => {
  const state = getState();
  const { healthFileLookup } = state;
  const { ssn } = healthFileLookup;
  dispatch(routeActions.push(`/healthfile/${ssn}`));
};