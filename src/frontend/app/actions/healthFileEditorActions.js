import http from 'util/http';
import { showNotification } from './notificationActions';
import { routeActions } from 'redux-simple-router';
import {setBackButtonAction} from './headerActions';
export const FETCH_HEALTH_FILE = 'healthFileEditor.FETCH_HEALTH_FILE';
export const FETCH_HEALTH_FILE_RESULT = 'healthFileEditor.FETCH_HEALTH_FILE_RESULT';

export const fetchHealthFile = (ssn) => async (dispatch, getState) => {
  dispatch({
    type: FETCH_HEALTH_FILE
  });

  const url = `/api/healthfiles/${ssn}`;
  const response = await http.get(url);
  let success, healthFile;
  if (response.status > 299) {
    success = false;
  } else {
    healthFile = response.data;
    success = true;
  }

  dispatch({
    type: FETCH_HEALTH_FILE_RESULT,
    success,
    healthFile
  });
};

/**
 * Medication
 */
let _medCounter = 1;

export const ADD_NEW_MEDICATION = 'healthFileEditor.ADD_NEW_MEDICATION';
export const addNewMedication = () => dispatch => dispatch({
  type: ADD_NEW_MEDICATION,
  id: `cid-${_medCounter++}`
});

export const UPDATE_MEDICATION = 'healthFileEditor.UPDATE_MEDICATION';
export const updateMedication = ({ id, name, instructions }) => ({
  type: UPDATE_MEDICATION,
  id,
  name,
  instructions
});

export const REMOVE_MEDICATION = 'healthFleEditor.REMOVE_MEDICATION';
export const removeMedication = (id) => ({
  type: REMOVE_MEDICATION,
  id
});

/**
 * Allergies
 */
let _allergyCounter = 1;

export const ADD_NEW_ALLERGY = 'healthFileEditor.ADD_NEW_ALLERGY';
export const addNewAllergy = () => dispatch => dispatch({
  type: ADD_NEW_ALLERGY,
  id: `cid-${_allergyCounter++}`
});

export const UPDATE_ALLERGY = 'healthFileEditor.UPDATE_ALLERGY';
export const updateAllergy = ({ id, name, effect, stage }) => ({
  type: UPDATE_ALLERGY,
  id,
  name,
  effect,
  stage
});

export const REMOVE_ALLERGY = 'healthFleEditor.REMOVE_ALLERGY';
export const removeAllergy = (id) => ({
  type: REMOVE_ALLERGY,
  id
});

export const SAVE_HEALTH_FILE = 'healthFileEditor.SAVE_HEALTH_FILE';
export const SAVE_HEALTH_FILE_RESULT = 'healthFileEditor.SAVE_HEALTH_FILE_RESULT';

export const saveHealthFile = () => async (dispatch, getState) => {
  dispatch({
    type: SAVE_HEALTH_FILE
  });

  const state = getState();
  const editor = state.healthFileEditor;
  const id = editor.healthFile.id;
  const data = {
    allergies: editor.allergies,
    medication: editor.medication
  };
  const url = `/api/healthfiles/${id}`;
  const response = await http.put(url, data);

  let success, healthFile;
  if (response.status > 299) {
    success = false;
  } else {
    success = true;
    healthFile = response.data;
  }

  dispatch({
    type: SAVE_HEALTH_FILE_RESULT,
    success,
    healthFile
  });

  if (success) {
    dispatch(showNotification('Saved health file'));
    dispatch(routeActions.push(`/healthfile/${id}`));
  }
};