import http from 'util/http';
import { routeActions } from 'redux-simple-router';

export const LOG_IN = 'LOG_IN';
export const LOG_IN_RESULT = 'LOG_IN_RESULT';

export const LOG_OUT = 'LOG_OUT';
export const LOG_OUT_RESULT = 'LOG_OUT_RESULT';


export const login = credentials => async dispatch => {
  dispatch({
    type: LOG_IN,
    credentials
  });

  const response = await http.post('/login', {
    ...credentials
  });

  if (response.status > 299) {
    dispatch({
      type: LOG_IN_RESULT,
      success: false
    });

    return;
  }

  const user = response.data;
  dispatch({ type: LOG_IN_RESULT, user, success: true });
  dispatch(routeActions.push('/'));
};

export const logout = () => async dispatch => {
  dispatch({
    type: LOG_OUT
  });

  const response = await http.post('/logout');

  if (response.status > 299) {
    dispatch({
      type: LOG_OUT_RESULT,
      success: false
    });

    return;
  }

  const user = response.data;
  dispatch({ type: LOG_OUT_RESULT, success: true });
  dispatch(routeActions.push('/login'));
};