export const SET_BACK_BUTTON_ACTION = 'header.SET_BACK_BUTTON_ACTION';

export const setBackButtonAction = action => ({
  type: SET_BACK_BUTTON_ACTION,
  backButtonAction: action
});

export const triggerBackButtonAction = () => (dispatch, getState) => {
  const {header} = getState();

  // Resets the action so the button does not show up anymore.
  dispatch(setBackButtonAction(null));

  // Triggers the configured action.
  dispatch(header.backButtonAction);
};