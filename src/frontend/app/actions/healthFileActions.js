import http from 'util/http';
import { routeActions } from 'redux-simple-router';
import {setBackButtonAction} from './headerActions';
export const FETCH_HEALTH_FILE = 'healthFile.FETCH_HEALTH_FILE';
export const FETCH_HEALTH_FILE_RESULT = 'healthFile.FETCH_HEALTH_FILE_RESULT';
import { fetchPatientRecords } from './patientRecordActions';
import { showNotification } from './notificationActions';

/**
 * Fetches the health file.
 * @param  {String} ssn) The Social Security Number to fetch a health file for.
 */
export const fetchHealthFile = (ssn) => async (dispatch, getState) => {
  if (!ssn) return;

  dispatch({
    type: FETCH_HEALTH_FILE
  });

  const url = `/api/healthfiles/${ssn}`;
  const response = await http.get(url);
  let success, healthFile;
  if (response.status > 299 || !response.data.personInfo) {
    success = false;
  } else {
    success = true;
    healthFile = response.data;
    if (healthFile.id) {
      dispatch(fetchPatientRecords(healthFile.id));
    }
  }

  // Now for the fun part: if the SSN equals the one from
  // the server, it means we did an SSN lookup, and so we want to
  // update the URL so it uses the ID instead.
  if (healthFile && healthFile.id && ssn.toUpperCase() === healthFile.ssn.toUpperCase()) {
    dispatch(routeActions.replace(`/healthfile/${healthFile.id}`));
  }

  dispatch({
    type: FETCH_HEALTH_FILE_RESULT,
    healthFile,
    success
  });
};

export const CREATE_HEALTH_FILE = 'healthFile.CREATE_HEALTH_FILE';
export const CREATE_HEALTH_FILE_RESULT = 'healthFile.CREATE_HEALTH_FILE_RESULT';

/**
 * Creates a health file.
 * @param  {String} ssn) The Social Security Number of the person to create the file for.
 */
export const createHealthFile = (ssn) => async (dispatch) => {
  dispatch({
    type: CREATE_HEALTH_FILE,
    ssn
  });

  const url = `/api/healthfiles/${ssn}`;
  const response = await http.post(url);
  let success, healthFile;
  if (response.status > 299) {
    success = false;
  } else {
    success = true;
    healthFile = response.data;
  }

  dispatch({
    type: CREATE_HEALTH_FILE_RESULT,
    success,
    healthFile
  });
  if (success) {
    dispatch(showNotification('Created new health file!'));
  }
};

export const editHealthFile = healthFileId => dispatch => {
  dispatch(routeActions.push(`/healthfile/${healthFileId}/edit`));
};