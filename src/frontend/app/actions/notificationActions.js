export const SHOW_NOTIFICATION = 'notifications.SHOW_NOTIFICATION';
export const HIDE_NOTIFICATION = 'notifications.HIDE_NOTIFICATION';

let timeout;

export const hideNotification = () => ({
  type: HIDE_NOTIFICATION
});

export const showNotification = message => dispatch => {
  dispatch({
    type: SHOW_NOTIFICATION,
    message
  });

  clearTimeout(timeout);
  timeout = setTimeout(() => dispatch(hideNotification()), 4000);
};