import React from 'react';
import { Provider } from 'react-redux';
import {render} from 'react-dom';
import Root from './containers/Root';
import configureStore from './store/configureStore';

const initialState = {
  // The current user is in the initial state.
  ...window.__INITIAL_STATE__
};

const store = configureStore(initialState);

render(
  <Root store={store}/>,
  document.getElementById('app')
);