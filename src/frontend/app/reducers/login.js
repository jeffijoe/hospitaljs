import { LOG_IN, LOG_IN_RESULT, LOG_OUT, LOG_OUT_RESULT } from 'actions/loginActions';

const initialState = {
  loading: false
};

export default function login(state = initialState, action) {
  switch (action.type) {
  case LOG_IN:
  case LOG_OUT:
    return {
      ...state,
      loading: true,
      success: null
    };
  case LOG_IN_RESULT:
  case LOG_OUT_RESULT:
    return {
      ...state,
      loading: false,
      success: action.success
    };
  }

  return state;
}

export default login;