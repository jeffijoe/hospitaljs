import {
  NEW_PATIENT_RECORD,
  CREATE_PATIENT_RECORD,
  CREATE_PATIENT_RECORD_RESULT,
  FETCH_PATIENT_RECORD,
  FETCH_PATIENT_RECORD_RESULT,
  EDIT_PATIENT_RECORD,
  SAVE_PATIENT_RECORD,
  SAVE_PATIENT_RECORD_RESULT,
  CANCEL_EDITING,
  CHANGE_PATIENT_RECORD,
  ADD_PROCEDURE,
  CHANGE_PROCEDURE,
  REMOVE_PROCEDURE,
  ADD_MEDIA,
  REMOVE_MEDIA,
  UPLOAD_PROGRESS,
  UPLOAD_COMPLETE,
  TOGGLE_BODYPART,
  PATIENT_RECORD_INVALID
} from 'actions/patientRecordActions';
import pickNoUndef from 'util/pickNoUndef';
import { update } from 'ramda';
import getMediaType from 'util/mediaTypes';


const initialState = {
  loading: true,
  saving: false,
  success: null,
  editing: false,
  record: null,
  validationErrors: {}
};

const pickNewData = pickNoUndef([
  'medication',
  'symptoms',
  'diagnosis',
  'bodyParts',
  'labResults'
]);

const getProcedure = (id, state) => state.find(x => x.id === id);
const cleanProcedures = (procedures) => procedures.filter(x => !!x.step);

function proceduresReducer (state = [], action) {
  switch (action.type) {
  case ADD_PROCEDURE:
    return [
      ...cleanProcedures(state),
      {
        id: action.id,
        dateTime: action.dateTime
      }
    ];
  case CHANGE_PROCEDURE:
    const existing = getProcedure(action.id, state);
    if (!existing) return state;
    const index = state.indexOf(existing);
    return update(
      index,
      {
        ...existing,
        step: action.step
      },
      state
    );
  case REMOVE_PROCEDURE:
    return state.filter(x => x.id !== action.id);
  }

  return state;
}

const getMedia = (id, state) => state.find(x => x.id === id);
function mediaReducer (state = [], action) {
  let media, index;
  switch (action.type) {
  case ADD_MEDIA:
    return [
      ...state,
      {
        id: action.id,
        fileName: action.fileName,
        uploading: true,
        uploaded: 0,
        total: 0
      }
    ];
  case REMOVE_MEDIA:
    return state.filter(x => x.id !== action.id);
  case UPLOAD_PROGRESS:
    media = getMedia(action.id, state);
    index = state.indexOf(media);
    return update(
      index,
      {
        ...media,
        total: action.total,
        uploaded: action.uploaded
      },
      state
    );
  case UPLOAD_COMPLETE:
    media = getMedia(action.id, state);
    index = state.indexOf(media);
    return update(
      index,
      {
        ...media,
        uploading: false,
        fileName: action.fileName,
        mediaType: getMediaType(action.fileType),
        fileSize: action.fileSize,
        fileUrl: action.fileUrl
      },
      state
    );
  }

  return state;
}

function bodyPartReducer (state = [], action) {
  switch (action.type) {
  case TOGGLE_BODYPART:
    if (state.find(x => x.bodyPart === action.bodyPartPath)) {
      return state.filter(x => x.bodyPart !== action.bodyPartPath);
    }
    return [
      ...state,
      {
        bodyPart: action.bodyPartPath
      }
    ];
  }
  /*case ADD_BODYPART:
    return [
      ...state,
      {
        bodyPart: action.bodyPartPath
      }
    ];
  case REMOVE_BODYPART:
    return state.filter(x => x.bodyPart !== action.bodyPartPath);
  }*/

  return state;
}

export default function patientRecordPageReducer (state = initialState, action) {
  switch (action.type) {
  case NEW_PATIENT_RECORD:
    return {
      ...state,
      isNew: true,
      editing: true,
      success: true,
      loading: false,
      healthFileId: action.healthFileId,
      record: {
        media: [],
        procedures: []
      }
    };
  case CREATE_PATIENT_RECORD:
    return {
      ...state,
      validationErrors: {},
      saving: true
    };
  case CREATE_PATIENT_RECORD_RESULT:
    return {
      ...state,
      saving: false,
      editing: false,
      record: {
        ...action.newRecord
      }
    };
  case FETCH_PATIENT_RECORD:
    return {
      ...state,
      loading: true,
      success: null
    };
  case FETCH_PATIENT_RECORD_RESULT:
    return {
      ...state,
      loading: false,
      success: action.success,
      editing: false,
      record: action.record
    };
  case EDIT_PATIENT_RECORD:
    return {
      ...state,
      editing: true,
      isNew: false,
      recordBeforeEditing: state.record
    };
  case SAVE_PATIENT_RECORD:
    return {
      ...state,
      validationErrors: {},
      saving: true,
      record: {
        ...state.record,
        procedures: cleanProcedures(state.record.procedures)
      }
    };
  case SAVE_PATIENT_RECORD_RESULT:
    return {
      ...state,
      saving: false,
      editing: false,
      success: action.success,
      recordBeforeEditing: null,
      ...(action.success && {
        record: {
          ...action.record
        }
      })
    };
  case CANCEL_EDITING:
    return {
      ...state,
      editing: false,
      record: state.recordBeforeEditing || {}
    };
  case CHANGE_PATIENT_RECORD:
    return {
      ...state,
      record: {
        ...state.record,
        ...pickNewData(action.newData)
      }
    };
  case PATIENT_RECORD_INVALID:
    return {
      ...state,
      validationErrors: action.validationErrors
    };
  }

  let newState = state;
  if (state.record) {
    const procedures = proceduresReducer(state.record.procedures, action);
    if (state.record.procedures !== procedures) {
      newState = {
        ...newState,
        record: {
          ...newState.record,
          procedures
        }
      };
    }
  }

  if (state.record) {
    const media = mediaReducer(state.record.media, action);
    if (state.record.media !== media) {
      newState = {
        ...newState,
        record: {
          ...newState.record,
          media
        }
      };
    }
  }

  if (state.record) {
    const bodyParts = bodyPartReducer(state.record.bodyParts, action);
    if (state.record.bodyParts !== bodyParts) {
      newState = {
        ...newState,
        record: {
          ...newState.record,
          bodyParts
        }
      };
    }
  }

  return newState;
}