import { SHOW_NOTIFICATION, HIDE_NOTIFICATION } from 'actions/notificationActions';

const initialState = {
  open: false,
  message: ''
};

export default function notificationsReducer (state = initialState, action) {
  switch(action.type) {
  case SHOW_NOTIFICATION:
    return {
      ...state,
      open: true,
      message: action.message
    };
  case HIDE_NOTIFICATION:
    return {
      ...state,
      open: false,
      message: ''
    };
  }

  return state;
}