import {
  FETCH_PATIENT_RECORDS,
  FETCH_PATIENT_RECORDS_RESULT
} from 'actions/patientRecordActions';

const initialState = {
  loading: false,
  records: []
};

export default function patientRecordsReducer (state = initialState, action) {
  switch (action.type) {
  case FETCH_PATIENT_RECORDS:
    return {
      ...state,
      loading: true,
      success: undefined,
      records: []
    };
  case FETCH_PATIENT_RECORDS_RESULT:
    return {
      ...state,
      loading: false,
      success: action.success,
      records: action.records
    };
  }

  return state;
}