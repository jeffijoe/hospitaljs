import { combineReducers } from 'redux';
import { routeReducer } from 'redux-simple-router';
import login from './login';
import currentUser from './currentUser';
import healthFileLookup from './healthFileLookup';
import healthFilePage from './healthFilePage';
import patientRecords from './patientRecords';
import approvalRequests from './approvalRequests';
import healthFileEditor from './healthFileEditor';
import notifications from './notifications';
import patientRecordPage from './patientRecordPage';
import header from './header';

export default combineReducers({
  login,
  currentUser,
  healthFileLookup,
  healthFilePage,
  patientRecords,
  patientRecordPage,
  approvalRequests,
  healthFileEditor,
  notifications,
  header,
  routing: routeReducer
});