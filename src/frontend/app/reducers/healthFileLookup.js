// We need the Action constant from the action creator
// so we can switch on it!
import { CHANGE_SSN } from 'actions/healthFileLookupActions';

// The initial state of the health file lookup.
const initialState = {
  ssn: ''
};

export default function healthFileLookupReducer (state = initialState, action) {
  // We switch on the action type to determine what we are "changing".
  switch (action.type) {
  case CHANGE_SSN:
    // Returns a new object based on the current state,
    // with a new ssn.
    //
    // Please note that we are not modifying anything, we are
    // returning a new object that is basically a clone of the previous
    // state, with the ssn set to the value specified in the action.
    return {
      // ES6 syntax for "spreading" an object on a new one, used for easy cloning.
      ...state,
      ssn: action.ssn
    };
  }

  // No actions matched the above, just return the current state.
  return state;
}