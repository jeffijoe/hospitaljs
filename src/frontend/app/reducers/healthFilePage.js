import {omit} from 'ramda';
import {
  FETCH_HEALTH_FILE,
  FETCH_HEALTH_FILE_RESULT,
  CREATE_HEALTH_FILE,
  CREATE_HEALTH_FILE_RESULT
} from 'actions/healthFileActions';

import {
  SAVE_HEALTH_FILE_RESULT
} from 'actions/healthFileEditorActions';


const omitPersonInfo = omit('personInfo');

const initialState = {
  healthFile: null,
  loading: true
};

export default function healthFilePageReducer (state = initialState, action) {
  switch (action.type) {
  case FETCH_HEALTH_FILE:
  case CREATE_HEALTH_FILE:
    return {
      ...state,
      loading: true,
      success: null
    };
  case FETCH_HEALTH_FILE_RESULT:
  case CREATE_HEALTH_FILE_RESULT:
  case SAVE_HEALTH_FILE_RESULT:
    const newState = {
      ...state,
      loading: false,
      success: action.success,
      healthFile: omitPersonInfo(action.healthFile)
    };

    if (action.healthFile && action.healthFile.personInfo) {
      newState.personInfo = action.healthFile.personInfo;
    }

    return newState;
  }

  return state;
}