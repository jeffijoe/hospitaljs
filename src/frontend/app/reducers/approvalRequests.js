import {
  FETCH_APPROVAL_REQUESTS,
  FETCH_APPROVAL_REQUESTS_RESULT,
  SET_VISIBILITY_FILTER,
  UPDATE_APPROVAL_REQUEST,
  UPDATE_APPROVAL_REQUEST_RESULT,
  TOGGLE_REJECTION_DIALOG,
  CHANGE_REJECTION_MESSAGE,
  REJECTION_VALIDATION_ERROR,
  VisibilityFilters
} from 'actions/approvalRequestActions';
import reduceReducers from 'reduce-reducers';
import updateIn from 'util/updateIn';

const initialState = {
  approvalRequests: [],
  loading: false,
  success: true,
  visibilityFilter: VisibilityFilters.SHOW_ALL,
  showRejectionDialog: false,
  rejectionMessage: null,
  requestToRejectId: null,
  rejectionErrorMessage: null
};

const fetchingReducer = (state = initialState, action) => {
  switch (action.type) {
  case FETCH_APPROVAL_REQUESTS:
    return {
      ...state,
      loading: true,
      success: null
    };
  case FETCH_APPROVAL_REQUESTS_RESULT:
    const newState = {
      ...state,
      loading: false,
      success: action.success,
      approvalRequests: action.approvalRequests
    };

    return newState;
  }
  return state;
};

const visibilityReducer = (state = initialState, action) => {
  switch (action.type) {
  case SET_VISIBILITY_FILTER:
    return {
      ...state,
      visibilityFilter: action.filter
    };
  }

  return state;
};

const updatingReducer = (state = initialState, action) => {
  switch(action.type) {
  case UPDATE_APPROVAL_REQUEST:
    return {
      ...state,
      rejectionErrorMessage: null,
      showRejectionDialog: false
    };
  case UPDATE_APPROVAL_REQUEST_RESULT:
    const newState = {
      ...state,
      success: action.success,
      approvalRequests: action.success
        ? updateIn(
            x => x.id === action.approvalRequest.id,
            cur => ({ ...action.approvalRequest }),
            state.approvalRequests
          )
        : state.approvalRequests,
      rejectionMessage: null,
      requestToRejectId: null
    };

    // Switches the filter.
    if (newState.success) {
      newState.visibilityFilter = VisibilityFilters.fromStatus(
        newState.approvalRequests.find(x => x.id === action.id).status
      );
    }

    return newState;
  }

  return state;
};

const rejectionReducer = (state = initialState, action) => {
  switch (action.type) {
  case TOGGLE_REJECTION_DIALOG:
    return {
      ...state,
      showRejectionDialog: action.show,
      requestToRejectId: action.requestToRejectId,
      ...!action.show && {
        rejectionMessage: null,
        rejectionErrorMessage: null
      }
    };
  case CHANGE_REJECTION_MESSAGE:
    return {
      ...state,
      rejectionMessage: action.rejectionMessage
    };
  case REJECTION_VALIDATION_ERROR:
    return {
      ...state,
      rejectionErrorMessage: action.errorMessage
    };
  }

  return state;
};

const approvalRequestsReducer = reduceReducers(
  fetchingReducer,
  visibilityReducer,
  updatingReducer,
  rejectionReducer
);

export default approvalRequestsReducer;