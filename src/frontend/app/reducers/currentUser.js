import { LOG_IN_RESULT, LOG_OUT_RESULT } from 'actions/loginActions';

const initialState = null;

export default function currentUserReducer (state = initialState, action) {
  switch(action.type) {
  case LOG_IN_RESULT:
    const {user} = action;
    return user || null;
  case LOG_OUT_RESULT:
    return action.success ? null : state;
  }

  return state;
}