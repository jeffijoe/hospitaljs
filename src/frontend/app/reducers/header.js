import { SET_BACK_BUTTON_ACTION } from 'actions/headerActions';

const initialState = {
  backButtonAction: null
};

export default function headerReducer (state = initialState, action) {
  switch (action.type) {
  case SET_BACK_BUTTON_ACTION:
    if (action.backButtonAction === null && state.backButtonAction === null) {
      return state;
    }

    return {
      ...state,
      backButtonAction: action.backButtonAction
    };
  }

  return state;
}