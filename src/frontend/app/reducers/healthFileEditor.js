import {
  FETCH_HEALTH_FILE,
  FETCH_HEALTH_FILE_RESULT,

  ADD_NEW_MEDICATION,
  UPDATE_MEDICATION,
  REMOVE_MEDICATION,

  ADD_NEW_ALLERGY,
  UPDATE_ALLERGY,
  REMOVE_ALLERGY,

  SAVE_HEALTH_FILE,
  SAVE_HEALTH_FILE_RESULT
} from 'actions/healthFileEditorActions';
import { update } from 'ramda';


const initialState = {
  loading: false,
  saving: false,
  success: null,
  healthFile: null,
  medication: [],
  allergies: []
};

const createUpdater = updaterFn => (state, action) => {
  const existing = state.find(x => x.id === action.id);
  if (!existing) return state;
  const position = state.indexOf(existing);
  return update(
    position,
    {
      id: action.id,
      ...updaterFn(existing, action)
    },
    state
  );
};

const updateMedication = createUpdater((state, action) => ({
  name: action.name,
  instructions: action.instructions
}));

const cleanMedication = (medication) => medication.filter(m => m.name || m.instructions);
const medicationReducer = (state = [], action) => {
  switch (action.type) {
  case ADD_NEW_MEDICATION:
    return [
      ...cleanMedication(state),
      {
        id: action.id
      }
    ];
  case UPDATE_MEDICATION:
    return updateMedication(state, action);
  case REMOVE_MEDICATION:
    return state.filter(x => x.id !== action.id);
  }
  return state;
};

const updateAllergy = createUpdater((state, action) => ({
  name: action.name,
  effect: action.effect,
  stage: action.stage
}));

const cleanAllergies = (allergy) => allergy.filter(m => m.name || m.effect);
const allergiesReducer = (state = [], action) => {
  switch (action.type) {
  case ADD_NEW_ALLERGY:
    return [
      ...cleanAllergies(state),
      {
        id: action.id
      }
    ];
  case UPDATE_ALLERGY:
    return updateAllergy(state, action);
  case REMOVE_ALLERGY:
    return state.filter(x => x.id !== action.id);
  }
  return state;
};

export default function healthFileEditorReducer (state = initialState, action) {
  switch (action.type) {
  case FETCH_HEALTH_FILE:
    return {
      ...state,
      loading: true
    };

  case FETCH_HEALTH_FILE_RESULT:
  case SAVE_HEALTH_FILE_RESULT:
    const healthFile = action.healthFile;
    return {
      ...state,
      loading: false,
      saving: false,
      success: action.success,
      healthFile,
      medication: (healthFile && healthFile.medication) || [],
      allergies: (healthFile && healthFile.allergies) || []
    };
  case SAVE_HEALTH_FILE:
    return {
      ...state,
      medication: cleanMedication(state.medication),
      allergies: cleanAllergies(state.allergies),
      saving: true
    };
  }

  const medication = medicationReducer(state.medication, action);
  if (medication !== state.medication) {
    return {
      ...state,
      medication
    };
  }

  const allergies = allergiesReducer(state.allergies, action);
  if (allergies !== state.allergies) {
    return {
      ...state,
      allergies
    };
  }

  return state;
}