import { createSelector } from 'reselect';
import { VisibilityFilters } from 'actions/approvalRequestActions';

// function selectApprovalRequests (approvalRequests, filter) {

// }

const selectApprovalRequests = createSelector(
  [state => state.reqs, state => state.visibilityFilter],
  (approvalRequests, filter) => {
    console.log('FILTERING!');

    switch (filter) {
    case VisibilityFilters.SHOW_ALL:
      return approvalRequests;
    case VisibilityFilters.SHOW_REJECTED:
      return approvalRequests.filter(approvalRequest => approvalRequest.status === 'rejected');
    case VisibilityFilters.SHOW_APPROVED:
      return approvalRequests.filter(approvalRequest => approvalRequest.status === 'approved');
    case VisibilityFilters.SHOW_PENDING:
      return approvalRequests.filter(approvalRequest => approvalRequest.status === 'pending');
    }
  }
);

const _filters = [
  {
    filter: 'SHOW_ALL',
    label: 'All'
  },
  {
    filter: 'SHOW_PENDING',
    label: 'Pending'
  },
  {
    filter: 'SHOW_APPROVED',
    label: 'Approved'
  },
  {
    filter: 'SHOW_REJECTED',
    label: 'Rejected'
  }
];

const visibilityFiltersSelector = createSelector(
  [approvalRequests => approvalRequests.approvalRequests],
  reqs => {
    const filtersToUse = [_filters[0]];
    if (!reqs || reqs.length === 0) return filtersToUse;
    const hasPending = reqs.some(x => x.status === 'pending');
    const hasApproved = reqs.some(x => x.status === 'approved');
    const hasRejected = reqs.some(x => x.status === 'rejected');
    if (hasPending) filtersToUse.push(_filters[1]);
    if (hasApproved) filtersToUse.push(_filters[2]);
    if (hasRejected) filtersToUse.push(_filters[3]);
    return filtersToUse;
  }
);

const EMPTY = [];
const requestsSelector = createSelector(
  [state => state.approvalRequests.approvalRequests, state => state.currentUser],
  (approvalRequests, currentUser) => {
    if (!approvalRequests || !currentUser) return EMPTY;
    return approvalRequests.map(
      x => ({
        ...x,
        canChangeStatus: currentUser.id !== x.requester.id
      })
    );
  }
);

export default createSelector(
  [
    state => state.approvalRequests,
    requestsSelector
  ],
  (approvalRequests, reqs) => {
    const filters = visibilityFiltersSelector(approvalRequests);
    const visibilityFilter = approvalRequests.visibilityFilter;

    const visibleApprovalRequests = selectApprovalRequests({reqs, visibilityFilter});
    return {
      ...approvalRequests,
      approvalRequests: visibleApprovalRequests,
      visibilityFilter,
      filters
    };
  }
);