import { createSelector } from 'reselect';

// record and recordsSelector don't do
// anything yet.

const recordSelector = createSelector(
  [record => record],
  record => {
    return {
      ...record,
      hasEnded: !!record.dateEnd
    };
  }
);

const recordsSelector = createSelector(
  patientRecords => patientRecords.records,
  records => records.map(recordSelector)
);

export default createSelector(
  [state => state.patientRecords],
  (patientRecords) => {
    const records = recordsSelector(patientRecords);
    return {
      ...patientRecords,
      records,
      hasRecords: records.length > 0
    };
  }
);