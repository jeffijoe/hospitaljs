import { createSelector } from 'reselect';
import patientRecordsSelector from './patientRecordsSelector';
import currentUserSelector from './currentUserSelector';

const pageSelector = createSelector(
  [state => state.healthFilePage],
  healthFilePage => {
    const hasHealthFile = healthFilePage.healthFile && !!healthFilePage.healthFile.id;
    return {
      ...healthFilePage,
      hasHealthFile
    };
  }
);

export default createSelector(
  [pageSelector, patientRecordsSelector, currentUserSelector],
  (healthFilePage, patientRecords, currentUser) => ({
    healthFilePage,
    patientRecords,
    addButtonVisible: currentUser.canWriteHealthfile,
    editButtonVisible: currentUser.canWriteHealthfile
  })
);