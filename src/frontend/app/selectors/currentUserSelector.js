import { createSelector } from 'reselect';
import permissions, { hasPermission } from 'util/permissions';

const currentUserSelector = createSelector(
  [state => state.currentUser],
  currentUser => {
    if (!currentUser) return { loggedIn: false };
    const currentPerms = currentUser.role.permissions;
    return {
      ...currentUser,
      loggedIn: true,
      ...currentUser && {
        canReadHealthfile: hasPermission(permissions.readHealthfile, currentPerms),
        canWriteHealthfile: hasPermission(permissions.writeHealthfile, currentPerms),
        canApproveHealthfile: hasPermission(permissions.approveHealthfile, currentPerms),
        canShareHealthfile: hasPermission(permissions.shareHealthfile, currentPerms)
      }
    };
  }
);

export default currentUserSelector;