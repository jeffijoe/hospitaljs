import { createSelector } from 'reselect';

export default createSelector(
  [state => state.healthFileLookup],
  healthFileLookup => {
    return {
      ...healthFileLookup,
      canOpen: !!healthFileLookup.ssn
    };
  }
);