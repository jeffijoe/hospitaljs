import { createSelector } from 'reselect';
import currentUserSelector from './currentUserSelector';

const hasSelector = createSelector(
  [page => page.record],
  record => ({
    hasAnyProcedures: false,
    hasAnyMedia: false,
    ...record && {
      hasAnyProcedures: record.procedures && record.procedures.length > 0,
      hasAnyMedia: record.media && record.media.length > 0
    }
  })
);

export default createSelector(
  [state => state.patientRecordPage, currentUserSelector],
  (page, currentUser) => {
    return {
      ...page,
      ...hasSelector(page),
      editButtonVisible: currentUser.canWriteHealthfile
    };
  }
);