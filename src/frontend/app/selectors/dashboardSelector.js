import approvalRequestsSelector from './approvalRequestsSelector';
import healthFileLookupSelector from './healthFileLookupSelector';
import currentUserSelector from './currentUserSelector';
import { createSelector } from 'reselect';

export default createSelector(
  [approvalRequestsSelector, healthFileLookupSelector, currentUserSelector],
  (approvalRequests, healthFileLookup, currentUser) => ({
    healthFileLookup,
    approvalRequests,
    inboxVisible: currentUser.canApproveHealthfile
  })
);