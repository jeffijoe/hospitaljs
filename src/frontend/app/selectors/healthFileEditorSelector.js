import { createSelector } from 'reselect';

export default createSelector(
  [state => state.healthFileEditor],
  editor => {
    return {
      ...editor
    };
  }
);