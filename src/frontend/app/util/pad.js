/**
 * Pads the the given value with 0's.
 * @param  {Any} val The value to pad - converted to a string.
 * @param  {Number} length The amount of padding to add.
 * @return {String}        The padded string.
 */
const pad = (val, length) => {
  let str = '' + val;
  while (str.length < length) {
    str = '0' + str;
  }

  return str;
};

export default pad;