import permissions, { getFriendlyPermissionName, hasPermission } from 'util/permissions';

export default function requireAuth(getState, requiredPermissions) {
  return (nextState, replace) => {
    const { currentUser } = getState();
    if (!currentUser) {
      replace({
        pathname: '/login',
        state: { nextPathname: nextState.location.pathname }
      });
      return;
    }

    // TODO: Redirect to /unauthorized when perms dont match.
    if (!hasPermission(requiredPermissions, currentUser.role.permissions)) {
      replace({
        pathname: '/unauthorized',
        state: {
          nextPathname: nextState.location.pathname,
          neededPerms: getFriendlyPermissionName(requiredPermissions)
        }
      });
    }
  };
}