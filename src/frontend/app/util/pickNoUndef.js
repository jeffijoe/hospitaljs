import { equals, pick, reject, compose, keys, filter, contains, curry } from 'ramda';

// const pickNoUndef = (keysToPick) => obj => {
//   const objKeys = keys(obj);
//   const wantedKeys = filter(x => contains(x, keysToPick))(objKeys);
//   const result = {};
//   wantedKeys.forEach(key => {
//     const value = obj[key];
//     if (value !== undefined) {
//       result[key] = value;
//     }
//   });

//   return result;
// };

/**
 * Returns a new object where there are no undefined values.
 */
const pickNoUndef = curry(compose(
  reject(equals(undefined)),
  pick
));


export default pickNoUndef;