export const PHOTO = 'photo';
export const VIDEO = 'video';
export const FILE = 'file';

const typeMap = [
  [/image/i, PHOTO],
  [/video/i, VIDEO]
];

const DEFAULT_TYPE = FILE;

/**
 * Returns the correct media type for a given file type.
 * @param  {String} fileType The file type.
 * @return {String}          The media type.
 */
export default function getMediaType (fileType) {
  const found = typeMap.find((m) => m[0].exec(fileType));
  return found ? found[1] : DEFAULT_TYPE;
}