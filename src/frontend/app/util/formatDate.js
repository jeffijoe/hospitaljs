import moment from 'moment';

export default function (date, fmt = 'LLL') {
  return moment(date).local().format(fmt);
}