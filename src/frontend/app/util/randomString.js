import {range} from 'ramda';

/**
 * Creates a random string of the given length.
 * @param {Number} length The length of the string to generate.
 * @return {String} The random string.
 */
export default function randomString (length) {
  // A string containing the allowed characters.
  const possibleChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  // Create an array of random characters.
  const chars = range(0, length)
                  .map(
                    x => possibleChars.charAt(
                      Math.floor(Math.random() * possibleChars.length)
                    )
                  );

  // Join the array to a string.
  return chars.join('');
}