import axios from 'axios';

const patched = {};

['request', 'get', 'post', 'put', 'delete', 'patch'].forEach(method => {
  patched[method] = function () {
    return axios[method](...arguments).then(
      result => result,
      err => {
        if (err instanceof Error) throw err;
        return err;
      }
    );
  };
});

export default patched;