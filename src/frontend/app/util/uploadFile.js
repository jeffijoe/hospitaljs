import pad from 'util/pad';
import http from 'util/http';
import {last} from 'ramda';

const noop = () => {};

const BLOCK_ID_PREFIX = 'block-';

/**
 * Uploads the file, returns a promise.
 * @param  {File} opts.file       File to upload.
 * @param  {String} opts.token    The token.
 * @param  {String} opts.endpoint Endpoint to upload to - this is the storage URL including the container.
 * @param  {Number} opts.blockSize The size of the block to upload.
 * @param  {Function} opts.progressCb An optional callback
 * @return {Promise}
 */
const uploadFile = ({
  file,
  token,
  endpoint,
  blockSize = 256 * 1024,
  progressCb = noop
}) => {

  return new Promise((resolve, reject) => {
    const fileName = file.name;
    const fileType = file.type;
    const fileSize = file.size;
    const blockIds = [];
    const reader = new FileReader();
    const fileUrl = `${endpoint}/${fileName}`;
    const submitUrl = `${fileUrl}?${token}`;
    let currentFilePointer = 0;
    let numberOfBlocks = 0;
    let totalBytesRemaining = fileSize;
    let bytesUploaded = 0;

    blockSize = Math.min(blockSize, fileSize);
    if (fileSize % blockSize === 0) {
      numberOfBlocks = fileSize / blockSize;
    } else {
      numberOfBlocks = parseInt(fileSize / blockSize, 10) + 1;
    }


    /**
     * Hands the block list to Azure, completing the Blob upload.
     */
    const commitBlockList = async () => {
      const url = `${submitUrl}&comp=blocklist`;
      const blockLines = blockIds.map(
        blockId => `<Latest>${blockId}</Latest>`
      ).join('\r\n');
      const requestBody = `<?xml version="1.0" encoding="utf-8"?>
                           <BlockList>
                           ${blockLines}
                           </BlockList> `;

      const response = await http.put(url, requestBody, {
        headers: {
          'x-ms-blob-content-type': fileType
        }
      });

      if (response.status > 299) {
        const err = new Error(`Commit failed (${response.status})`);
        err.response = response;
        reject(err);
      }

      resolve({
        fileName,
        fileSize,
        fileType,
        fileUrl,
        response
      });
    };

    /**
     * Reads the next portion of the file.
     * This will trigger the file reader.
     */
    const readNextBlock = async () => {
      if (totalBytesRemaining > 0) {
        const fileContent = file.slice(currentFilePointer, currentFilePointer + blockSize);
        const blockId = BLOCK_ID_PREFIX + pad(blockIds.length, 6);
        blockIds.push(btoa(blockId));

        reader.readAsArrayBuffer(fileContent);
        currentFilePointer += blockSize;
        totalBytesRemaining -= blockSize;

        // When we're almost done, set the block size to the remaining byte amount.
        if (totalBytesRemaining < blockSize) {
          blockSize = totalBytesRemaining;
        }
      } else {
        await commitBlockList();
      }
    };

    /**
     * File reader event.
     */
    const onReadEvent = async e => {
      if (e.target.readyState !== FileReader.DONE) {
        return;
      }

      const url = `${submitUrl}&comp=block&blockid=${last(blockIds)}`;
      const requestData = new Uint8Array(e.target.result);
      const response = await http.put(url, requestData, {
        headers: {
          'x-ms-blob-content-type': 'BlockBlob'
        }
      });

      if (response.status > 299) {
        const err = new Error(`Commit failed (${response.status})`);
        err.response = response;
        reject(err);
        return;
      }

      bytesUploaded += requestData.length;
      progressCb({
        total: fileSize,
        uploaded: bytesUploaded
      });

      readNextBlock();
    };

    reader.addEventListener('loadend', onReadEvent);
    readNextBlock();
  });
};

export default uploadFile;