export const UNITS = [
  'bytes',
  'kilobytes',
  'megabytes',
  'gigabytes',
  'terabytes'
];

export const SHORT_UNITS = [
  'bytes',
  'kb',
  'mb',
  'gb',
  'tb'
];

/**
 * Given a unit, returns the factor that should be used for division.
 */
export function getFactorForUnit (unit) {
  let index = UNITS.indexOf(unit);
  if (index === -1) {
    throw new Error(`Unknown size '${unit}'`);
  }

  if (index === 0) {
    return 1;
  }

  let power = index;
  return Math.pow(1024, power);
}

/**
 * Given a byte count, the unit that makes most sense.
 * @param {Number} sizeInBytes The size in bytes.
 */
export function sizeUnit (sizeInBytes) {
  if (sizeInBytes === undefined) {
    throw new Error('sizeInBytes cannot be undefined');
  }

  let factor = 1024;
  for (var i = 0; i < UNITS.length; i++) {
    if (sizeInBytes < factor) {
      let unit = UNITS[i];
      return unit;
    }

    factor *= 1024;
  }

  throw new Error('Out of range, holy shit.');
}

/**
 * Given a byte count, returns a string
 * @param {Number} sizeInBytes The size in bytes.
 * @param {Boolean} round If true, will round the result. Default is false.
 */
export function sizeString (sizeInBytes, round = false) {
  let unit = sizeUnit(sizeInBytes);

  if (unit === 'bytes') {
    return `${sizeInBytes} bytes`;
  }
  let shortUnit = SHORT_UNITS[UNITS.indexOf(unit)];
  let factor = getFactorForUnit(unit);
  let result = sizeInBytes / factor;
  result = round ? Math.round(result) : result.toFixed(2);
  result = result.toString().replace(/\.00$/, '');
  return `${result}${shortUnit}`;
}