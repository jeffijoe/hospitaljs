import permissions, { getFriendlyPermissionName, hasPermission } from 'babel!../../../backend/util/permissions';

export { getFriendlyPermissionName, hasPermission };
export default permissions;