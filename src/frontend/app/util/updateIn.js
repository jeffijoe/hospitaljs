import {update, curry} from 'ramda';

/**
 * Functional updater function.
 * @param  {Function} predicate The predicate to find the value to update.
 * @param  {Function} newValueFn  A function to invoke with the old value, which returns the new value.
 * @param  {Array} source    The source array.
 * @return {Array}           The updated array.
 */
function updateIn (predicate, newValueFn, source) {
  const currentValue = source.find(predicate);
  if (currentValue === undefined) return source;
  const index = source.indexOf(currentValue);
  const newValue = newValueFn(currentValue);
  return update(index, newValue, source);
}

export default curry(updateIn);