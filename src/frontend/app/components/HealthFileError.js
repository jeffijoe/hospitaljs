import React from 'react';
import { Link } from 'react-router';
import Heading from './Heading';
import Colors from 'material-ui/lib/styles/colors';
import RaisedButton from 'material-ui/lib/raised-button';

export default function HealthFileError () {
  return (
    <div style={{ textAlign: 'center', paddingTop: 40 }}>
      <Heading thin={true} color={Colors.red500}>
        No person information found for the specified SSN.
      </Heading>
      <RaisedButton
        secondary={true}
        label='Back to dashboard'
        containerElement={<Link to='/' />}
        linkButton={true}
      />
    </div>
  );
}