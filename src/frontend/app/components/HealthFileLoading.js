import React from 'react';
import CircularProgress from 'material-ui/lib/circular-progress';
import styles from 'styles/HealthFileLoading.scss';


export default function HealthFileLoading () {
  return (
    <div className={styles.container}>
      <CircularProgress/>
      <div className={styles.loading}>
        Loading file, please wait...
      </div>
    </div>
  );
}