import React from 'react';
import styles from 'styles/NoHealthFile.scss';
import Heading from './Heading';
import RaisedButton from 'material-ui/lib/raised-button';

const NoHealthFile = ({ personFirstName, personLastName, create, ssn }) => (
  <div className={styles.container}>
    <Heading thin={true}>
      {personFirstName} {personLastName} has no health file.
    </Heading>
    <RaisedButton
      id='createHealthfile'
      secondary={true}
      label='Create health file now'
      onTouchTap={() => create(ssn)}
    />
  </div>
);

export default NoHealthFile;