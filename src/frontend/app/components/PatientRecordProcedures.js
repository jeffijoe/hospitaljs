import React from 'react';
import RaisedButton from 'material-ui/lib/raised-button';
import styles from 'styles/PatientRecordProcedures.scss';
import formatDate from 'util/formatDate';
import Paper from 'material-ui/lib/paper';
import Row from './Row';
import Column from './Column';
import IconButton from 'material-ui/lib/icon-button';
import Delete from 'material-ui/lib/svg-icons/action/delete';

const Procedure = ({ editing, saving, procedure, change, remove }) => {
  if (editing) {
    let input;
    const onChange = () => {
      change({ id: procedure.id, step: input.value });
    };
    return (
      <Paper className={styles.editor}>
        <Row responsive={false} alignItems='center'>
          <Column flex='1 1 auto' className={styles.inputContainer}>
            <input
              type='text'
              className={styles.input}
              disabled={saving}
              placeholder='Procedure text here.'
              value={procedure.step}
              onChange={onChange}
              ref={x => input = x}
            />
          </Column>
          <Column flex='0 0 auto'>
            <IconButton
              disabled={saving}
              onClick={() => remove(procedure.id)}
            >
              <Delete/>
            </IconButton>
          </Column>
        </Row>
      </Paper>
    );
  } else {
    return (
      <div className={styles.view}>
        <div className={styles.dateAdded}>
          {formatDate(procedure.dateTime)}
        </div>
        <div className={styles.step}>
          {procedure.step}
        </div>
      </div>
    );
  }

};

export default function PatientRecordProcedures ({
  editing,
  saving,
  procedures,
  add,
  change,
  remove
}) {
  return (
    <div>
      <div className={styles.procedures}>
        {procedures.map(procedure =>
          <Procedure
            key={procedure.id}
            procedure={procedure}
            editing={editing}
            saving={saving}
            change={change}
            remove={remove}
          />
        )}
      </div>
      {editing &&
        <RaisedButton
          disabled={saving}
          label='Add procedure'
          onClick={() => add()}
        />
      }
    </div>
  );
}