import React from 'react';
import FloatingActionButton from 'material-ui/lib/floating-action-button';
import ModeEdit from 'material-ui/lib/svg-icons/editor/mode-edit';
import Save from 'material-ui/lib/svg-icons/content/save';
import styles from 'styles/PatientRecordPageActions.scss';
import Cancel from 'material-ui/lib/svg-icons/navigation/cancel';

export default function PatientRecordPageActions ({
  editing,
  saving,
  edit,
  save,
  cancel
}) {
  return (
    <div className={styles.container}>
      {editing &&
        <FloatingActionButton
          secondary={true}
          disabled={saving}
          onClick={() => cancel()}
        >
          <Cancel/>
        </FloatingActionButton>
      }
      <FloatingActionButton
        primary={editing}
        disabled={saving}
        style={{marginLeft: 10}}
        secondary={!editing}
        onClick={() => editing ? save() : edit()}
      >
        {editing ? <Save/> : <ModeEdit/>}
      </FloatingActionButton>
    </div>
  );
}