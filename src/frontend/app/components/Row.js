import React from 'react';
import styles from 'styles/Row.scss';
import cx from 'classnames';

/**
 * Row component.
 * @param  {Array<ReactComponent>} options.children The children.
 */
const Row = ({ children, pad, style, alignItems, className, responsive }) => {
  const newStyle = {
    alignItems,
    ...style
  };
  return (
    <div
      className={cx(
        styles.row,
        pad && styles.pad,
        responsive && styles.responsive,
        className
      )}
      style={newStyle}
    >
      {children}
    </div>
  );
};

export default Row;