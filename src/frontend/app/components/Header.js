import React from 'react';
import CurrentUser from './CurrentUser';
import AppBar from 'material-ui/lib/app-bar';
import IconButton from 'material-ui/lib/icon-button';
import ArrowBack from 'material-ui/lib/svg-icons/navigation/arrow-back';
import DashboardIcon from 'material-ui/lib/svg-icons/action/dashboard';

export default function Header(props) {
  const {
    currentUser,
    logout,
    onTitleClick,
    onBackClick
  } = props;
  return (
    <AppBar
      title='Hospital.js'
      showMenuIconButton={true}
      iconElementLeft={
        <div>
          <IconButton onClick={() => onBackClick ? onBackClick() : onTitleClick()}>
            {onBackClick
              ? <ArrowBack color='#fff'/>
              : <DashboardIcon color='#fff'/>
            }
          </IconButton>
        </div>
      }
      iconElementRight={currentUser &&
        <CurrentUser user={currentUser} logout={logout} />
      }
    />
  );
}