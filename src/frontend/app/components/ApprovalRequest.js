import React from 'react';
import Paper from 'material-ui/lib/paper';
import styles from 'styles/ApprovalRequest.scss';
import CheckBoxOutline from 'material-ui/lib/svg-icons/toggle/check-box-outline-blank';
import Container from './Container';
import CheckBox from 'material-ui/lib/checkbox';
import Row from './Row';
import Column from './Column';
import formatDate from 'util/formatDate';
import cx from 'classnames';
import IconMenu from 'material-ui/lib/menus/icon-menu';
import MenuItem from 'material-ui/lib/menus/menu-item';
import MoreIcon from 'material-ui/lib/svg-icons/navigation/more-vert';
import Check from 'material-ui/lib/svg-icons/navigation/check';
import ErrorOutline from 'material-ui/lib/svg-icons/alert/error-outline';
import Launch from 'material-ui/lib/svg-icons/action/launch';
import Colors from 'material-ui/lib/styles/colors';
import IconButton from 'material-ui/lib/icon-button';
import Divider from 'material-ui/lib/divider';

export default function ApprovalRequest (props) {
  const {
    request,
    view,
    approve,
    reject
  } = props;
  const statusClass = styles[request.status];
  const viewIcon = <Launch />;
  const approveIcon = <Check color={Colors.green500} />;
  const rejectIcon = <ErrorOutline color={Colors.red500} />;
  const isPending = request.status === 'pending';
  const menuButton = (
    <IconButton>
      <MoreIcon/>
    </IconButton>
  );
  return (
    <Paper>
      <Row alignItems='stretch'>
        <Column flex='0 0 auto' style={{position: 'relative'}}>
          <div className={statusClass}>
            &nbsp;
          </div>
        </Column>
        <Column flex='1 1 auto'>
          <Row alignItems='center' className={styles.row}>
            <Column flex='0 0 auto'>
              <div className={styles.diagnose}>
                {request.patientRecord.diagnosis}
              </div>
            </Column>
            <Column flex='0 0 auto'>
              <div className={styles.doctor}>
                &mdash;
                &nbsp;
                {request.requester.firstName} {request.requester.lastName}
              </div>
            </Column>
            {request.status === 'rejected' && request.rejectedReason &&
              <Column flex='0 0 auto'>
                <div className={styles.rejectionReason}>
                  <span className={styles.rejectedLabel}>Rejected</span>:{' '}
                  {request.rejectedReason}
                </div>
              </Column>
            }
            <Column flex='1 1 auto'>
              <div className={styles.timestamp}>
                {formatDate(request.requestedAt)}
              </div>
            </Column>
            <Column flex='0 0 auto'>
              <IconMenu
                iconButtonElement={menuButton}
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
              >
                <MenuItem
                  primaryText='View'
                  leftIcon={viewIcon}
                  onClick={() => view(request.patientRecord.id)}
                />
                {isPending && request.canChangeStatus &&
                  <div>
                    <Divider></Divider>
                    <MenuItem
                      primaryText='Approve'
                      leftIcon={approveIcon}
                      onClick={() => approve(request.id)}
                    />
                    <MenuItem
                      primaryText='Reject'
                      leftIcon={rejectIcon}
                      onClick={() => reject(request.id)}
                    />
                  </div>
                }
              </IconMenu>
            </Column>
          </Row>
        </Column>
      </Row>
    </Paper>
  );
}