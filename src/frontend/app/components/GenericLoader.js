import React from 'react';
import CircularProgress from 'material-ui/lib/circular-progress';

export default function GenericLoader ({text = 'Loading...'}) {
  return (
    <div style={{textAlign: 'center', padding: 10 }}>
      <CircularProgress />
      <div style={{ color: '#999', padding: '10px 0'}}>
        {text}
      </div>
    </div>
  );
}