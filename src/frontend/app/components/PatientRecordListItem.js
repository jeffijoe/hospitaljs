import React from 'react';
import Paper from 'material-ui/lib/paper';
import styles from 'styles/PatientRecordListItem.scss';
import formatDate from 'util/formatDate';
import IconMenu from 'material-ui/lib/menus/icon-menu';
import MenuItem from 'material-ui/lib/menus/menu-item';
import IconButton from 'material-ui/lib/icon-button';
import MoreVert from 'material-ui/lib/svg-icons/navigation/more-vert';
import Launch from 'material-ui/lib/svg-icons/action/launch';

export default function PatientRecordListItem ({ record, view }) {
  return (
    <Paper className={styles.container}>
      <div className={styles.diagnosis}>
        {record.diagnosis}
      </div>
      <div className={styles.dates}>
        {formatDate(record.dateStart, 'LL')}
        {record.hasEnded &&
          <span>
            &nbsp;
            &mdash;
            &nbsp;
            {formatDate(record.dateEnd, 'LL')}
          </span>
        }
      </div>
      <div className={styles.actions}>
        <IconMenu
          iconButtonElement={<IconButton><MoreVert/></IconButton>}
          targetOrigin={{horizontal: 'right', vertical: 'top'}}
          anchorOrigin={{horizontal: 'right', vertical: 'top'}}
        >
          <MenuItem
            primaryText='View'
            leftIcon={<Launch/>}
            onClick={() => view(record.id)}
          />
        </IconMenu>
      </div>
    </Paper>
  );
}