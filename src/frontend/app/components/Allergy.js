import React from 'react';
import Paper from 'material-ui/lib/paper';
import styles from 'styles/Allergy.scss';
import AllergyStage from './AllergyStage';

/**
 * Allergy component displays information about a particular allergy.
 * @param {Object} options.allergy     The allergy object to display.
 * @param {Function} options.changeStage A callback to call when the
 *                                     stage of the allergy wants to be changed.
 */
export default function Allergy ({ allergy, changeStage }) {
  const _changeStage = stage => changeStage(stage, allergy.id);
  return (
    <Paper className={styles.container}>
      <div className={styles.info}>
        <div className={styles.name}>
          {allergy.name}
        </div>
        <div className={styles.effect}>
          Effect: {allergy.effect}
        </div>
      </div>

      <div className={styles.stage}>
        <AllergyStage
          readOnly={true}
          stage={allergy.stage}
          changeStage={_changeStage}
        />
      </div>
    </Paper>
  );
}