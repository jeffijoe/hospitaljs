import React from 'react';
import Colors from 'material-ui/lib/styles/colors';
import IconButton from 'material-ui/lib/icon-button';
import { range, map } from 'ramda';

const Circle = ({ size = 24, color }) => {
  const half = Math.floor(size / 2);
  return (
    <svg width={size} height={size}>
      <circle
        cx={half}
        cy={half}
        r={half}
        fill={color}
      />
    </svg>
  );
};

const StageEntry = ({filled, readOnly, onClick, disabled}) => {
  const circleSize = 27;
  const color = filled ? Colors.orange500 : Colors.orange100;
  const circle = <Circle size={circleSize} color={color} />;
  return (
    !readOnly
      ? <IconButton onClick={onClick} disabled={disabled}>{circle}</IconButton>
      : (
        <div style={{padding: '0 5px', display: 'inline-block'}}>
          {circle}
        </div>
      )

  );
};

export default function AllergyStage ({ stage, readOnly, changeStage, disabled }) {
  const stages = range(1, 6).map(s => ({
    filled: stage >= s,
    stage: s
  }));


  return (
    <div>
      {stages.map(s =>
        <StageEntry
          key={s.stage} {...s}
          readOnly={readOnly}
          onClick={() => changeStage(s.stage)}
          disabled={disabled}
        />
      )}
    </div>
  );
}