import React, { Component } from 'react';
import styles from 'styles/LoginForm.scss';
import Paper from 'material-ui/lib/paper';
import FlatButton from 'material-ui/lib/flat-button';
import TextField from 'material-ui/lib/text-field';
import CircularProgress from 'material-ui/lib/circular-progress';

const Group = ({ children }) => (
  <div className={styles.group}>
    {children}
  </div>
);


export default class LoginForm extends Component {
  constructor() {
    super(...arguments);
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.onLoginClick = this.onLoginClick.bind(this);
    this.state = {
      canLogIn: false,
      employeeId: '',
      password: ''
    };
  }

  render() {
    const props = this.props;
    const buttonStyles = {
      width: '100%'
    };

    const { canLogin } = this.state;

    const { isLoading, success } = props;

    const textFieldProps = {
      fullWidth: true,
      onChange: this.handleFieldChange.bind(this),
      onKeyUp: e => e.keyCode === 13 && this.onLoginClick()
    };

    let loginArea;
    if (isLoading) {
      loginArea = (
        <div className={styles.loading}>
          <CircularProgress/>
        </div>
      );
    } else if (success === false) {
      loginArea = (
        <div className={styles.badLogin}>
          That didn't go so well. Type your stuff right this time, please?
        </div>
      );
    }

    return (
      <div className={styles.container}>
        <h1 className={styles.title}>
          User Login
        </h1>

        <Paper className={styles.form}>
          <Group>
            <TextField
              value={this.state.employeeId}
              ref='employeeId'
              floatingLabelText='Employee ID'
              id='username'
              {...textFieldProps}
            />
          </Group>
          <Group>
            <TextField
              value={this.state.password}
              ref='password'
              floatingLabelText='Password'
              type='password'
              id='password'
              {...textFieldProps}
            />
          </Group>
          <Group>
            {!isLoading &&
              <FlatButton
                label='Log in'
                id='login'
                primary={true}
                style={buttonStyles}
                onClick={this.onLoginClick}
                disabled={!canLogin || isLoading}
              />
            }
          </Group>
          <Group>
            {loginArea}
          </Group>
        </Paper>
      </div>
    );
  }

  handleFieldChange() {
    const state = {
      employeeId: this.refs.employeeId.getValue(),
      password: this.refs.password.getValue()
    };

    const canLogin = state.employeeId && state.password;

    this.setState({
      ...state,
      canLogin
    });
  }

  onLoginClick() {
    this.props.onSubmit({
      employeeId: this.state.employeeId,
      password: this.state.password
    });
  }
}