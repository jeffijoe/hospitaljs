import React from 'react';
import Heading from './Heading';
import styles from 'styles/HealthFileLookup.scss';
import TextField from 'material-ui/lib/text-field';
import Paper from 'material-ui/lib/paper';
import RaisedButton from 'material-ui/lib/raised-button';
import Row from './Row';
import Column from './Column';
import CircularProgress from 'material-ui/lib/circular-progress';
import IconButton from 'material-ui/lib/icon-button';
import ArrowForward from 'material-ui/lib/svg-icons/navigation/arrow-forward';

/**
 * Health file lookup component.
 */
export default function HealthFileLookup(props) {

  let input;
  const { ssn, canOpen, loading } = props.healthFileLookup;
  const onChange = () => {
    props.onChangeSsn(input.value);
  };

  const onOpenClick = () => {
    props.onOpen();
  };

  return (
    <div className={styles.container}>
      <Paper>
        <div className={styles.inputContainer}>
          <Row alignItems='center' pad={true}>
            <Column flex='1 1 auto'>

              <input
                type='text'
                id='search'
                // The value is the current SSN.
                value={ssn}
                placeholder='SSN with prefix - e.g. DK1201800215'
                className={styles.searchField}
                // Triggers an action when the text changes.
                onChange={onChange}
                ref={x => input = x}
                onKeyUp={e => e.keyCode === 13 && onOpenClick()}
              />

            </Column>
            <Column flex='0 0 auto'>
              <IconButton
                primary={true}
                disabled={!canOpen}
                label='Open'
                id='open'
                onClick={onOpenClick}>
                <ArrowForward />
              </IconButton>
            </Column>
          </Row>
        </div>
      </Paper>
    </div>
  );
}