import React from 'react';
import styles from 'styles/Column.scss';
import cx from 'classnames';

/**
 * Column component.
 * @param  {Array<ReactComponent>} options.children The children.
 */
const Column = ({
  style,
  children,
  flex = '1 1 auto',
  className
}) => {
  const rootStyles = {
    flex,
    ...style
  };
  return (
    <div className={cx(styles.column, className)} style={rootStyles}>
      {children}
    </div>
  );
};

export default Column;