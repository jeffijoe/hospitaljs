import React from 'react';
import styles from 'styles/Footer.scss';

/**
 * Application Footer
 * @param {[type]} options.year The copyright year.
 */
export default function Footer({ year }) {
  return (
    <div className={styles.footer}>
      Copyright &copy; Solutions.js {year}. All rights reserved.
    </div>
  );
}