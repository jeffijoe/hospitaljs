import React from 'react';
import FlatButton from 'material-ui/lib/flat-button';

export default function ApprovalRequestsVisibilityFilters ({onChangeVisibilityFilter, current, filters }) {
  return (
    <div>
      {filters.map(filter =>
        <FlatButton
          key={filter.filter}
          label={filter.label}
          disabled={current === filter.filter}
          onClick={() => onChangeVisibilityFilter(filter.filter)}
        />
      )}
    </div>
  );
}