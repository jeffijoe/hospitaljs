import React from 'react';
import styles from 'styles/Container.scss';

/**
 * Container component.
 * @param  {Array<ReactComponent>} options.children The children.
 */
const Container = ({ children, style }) => {
  return (
    <div className={styles.container} style={style}>
      {children}
    </div>
  );
};

export default Container;