import React from 'react';
import styles from 'styles/PatientRecordMediaList.scss';
import IconButton from 'material-ui/lib/icon-button';
import Delete from 'material-ui/lib/svg-icons/action/delete';
import FileDownload from 'material-ui/lib/svg-icons/file/file-download';
import FileUpload from 'material-ui/lib/svg-icons/file/file-upload';
import LinearProgress from 'material-ui/lib/linear-progress';
import { PHOTO, VIDEO } from 'util/mediaTypes';
import FileIcon from 'material-ui/lib/svg-icons/editor/insert-drive-file';
import {sizeString} from 'util/size';

const PhotoMedia = ({ media }) => {
  return (
    <img
      src={media.fileUrl}
      className={styles.image}
    />
  );
};

const VideoMedia = ({ media }) => {
  return (
    <div className={styles.videoContainer}>
      <video src={media.fileUrl} className={styles.video} controls />
    </div>
  );
};

const FileMedia = ({ media }) => {
  const ICON_SIZE = 64;
  return (
    <div className={styles.fileMedia}>
      <div>
        <FileIcon style={{width:ICON_SIZE, height:ICON_SIZE}} />
      </div>
      <div className={styles.fileSize}>{sizeString(media.fileSize)}</div>
    </div>
  );
};

const mediaElementFor = media => {
  switch (media.mediaType) {
  case PHOTO: return <PhotoMedia media={media} />;
  case VIDEO: return <VideoMedia media={media} />;
  default: return <FileMedia media={media} />;
  }
};

/**
 * Media being displayed.
 */
const Media = ({ media, editing, remove, saving }) => (
  <div className={styles.relative}>
    {mediaElementFor(media)}
    <div className={styles.meta}>
      <div className={styles.info}>
        <div className={styles.fileName}>{media.fileName}</div>
      </div>
      <div className={styles.actions}>
        {/*Download button*/}
        <a href={media.fileUrl} target='_blank'>
          <IconButton>
            <FileDownload color='#fff'/>
          </IconButton>
        </a>

        {editing &&
          /*Delete button*/
          <IconButton
            onClick={() => remove(media.id)}
            disabled={saving}
          >
              <Delete color='#fff'/>
          </IconButton>
        }
      </div>
    </div>
  </div>
);

/**
 * Displays upload progress.
 */
const UploadProgress = ({ media }) => {
  return (
    <div>
      <LinearProgress
        min={0}
        max={media.total}
        value={media.uploaded}
        mode={media.total ? 'determinate' : 'indeterminate'}
      />
    </div>
  );
};

/**
 * An upload button. Nice.
 * @param  {Function} options.upload The callback that receives the file.
 */
const UploadButton = ({ upload, disabled }) => {
  const doUpload = e => {
    const file = e.target.files[0];
    upload(file);
  };
  return (
    <div className={styles.addContainer}>
      <div className={styles.addContainerInner}>
        <label>
          <FileUpload style={{ width: 48, height: 48 }}/>
          <input
            type='file'
            accept='*'
            value=''
            className={styles.fileInput}
            onChange={doUpload}
            disabled={disabled}
          />
        </label>
      </div>
    </div>
  );
};

/**
 * Displays a list of media.
 * @param {Bool} options.editing Are we editing?
 * @param {Bool} options.saving  Are we saving?
 * @param {Object} options.media   The media data.
 * @param {Function} options.add     A callback for when we want to add more.
 * @param {Function} options.remove  A callback for when a specific media wants to be removed.
 */
export default function PatientRecordMediaList ({
  editing,
  saving,
  media,
  add,
  remove
}) {
  return (
    <div className={styles.container}>
      <div className={styles.grid}>
        {media.map(m =>
          <div
            className={styles.media}
            key={m.id}
          >
            {m.uploading
              ? <UploadProgress media={m} />
              : <Media
                  media={m}
                  editing={editing}
                  saving={saving}
                  remove={remove}
                />
            }
          </div>
        )}

        {editing &&
          <UploadButton
            upload={add}
            disabled={saving}
          />
        }
      </div>
    </div>
  );
}