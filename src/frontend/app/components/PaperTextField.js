import React from 'react';
import Paper from 'material-ui/lib/paper';
import styles from 'styles/PaperTextField.scss';
import Textarea from 'react-textarea-autosize';
import nl2br from 'react-nl2br';
import cx from 'classnames';

export default function PaperTextField ({
  value,
  editing,
  change,
  saving,
  error
}) {
  let input;
  const onChange = () => {
    change(input.value);
  };

  return (
    <Paper className={styles.container}>
      {editing &&
        <div className={styles.editing}>
          <Textarea
            className={cx(styles.textField, { hasError: !!error })}
            value={value}
            onChange={onChange}
            disabled={saving}
            placeholder='You can enter text here!'
            ref={x => input = x}
          />
          {!!error &&
            <div className={styles.error}>
              {error}
            </div>
          }
        </div>
      }
      {!editing &&
        <div className={styles.view}>
          <div>
            {nl2br(value)}
          </div>
        </div>
      }
    </Paper>
  );
}