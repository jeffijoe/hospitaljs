import React from 'react';
import Heading from './Heading';
import RaisedButton from 'material-ui/lib/raised-button';
import styles from 'styles/ItemsEditor.scss';

export default function ItemsEditor ({
  title,
  add,
  remove,
  items,
  change,
  itemEditor,
  disabled,
  id
}) {
  const Editor = itemEditor;
  return (
    <div id={id} className={styles.container}>
      <Heading thin={true}>{title}</Heading>

      <div className={styles.itemsContainer}>
        {items.map(m =>
          <div data-pioneer='item editor' key={m.id}>
            <Editor
              item={m}
              change={change}
              remove={remove}
              disabled={disabled}
            />
          </div>
        )}
      </div>

      <div>
        <RaisedButton
          label='Add'
          data-pioneer='add item'
          disabled={disabled}
          onClick={() => add()}
        />
      </div>
    </div>
  );
}