import React from 'react';

export default function Heading (props) {
  props = {
    thin: true,
    ...props
  };

  const styles = {
    ...(props.thin && { fontWeight: '300' }),
    ...(props.color && { color: props.color }),
    ...props.styles
  };

  return <h1 style={styles} {...props}>{props.children}</h1>;
}