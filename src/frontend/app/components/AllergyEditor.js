import React from 'react';
import Paper from 'material-ui/lib/paper';
import styles from 'styles/AllergyEditor.scss';
import TextField from 'material-ui/lib/text-field';
import FlatButton from 'material-ui/lib/flat-button';
import AllergyStage from './AllergyStage';

export default function AllergyEditor ({
  item,
  change,
  remove,
  disabled
}) {
  let nameField, effectField, stage = item.stage;

  const onChange = () => {
    change({
      id: item.id,
      name: nameField.getValue(),
      effect: effectField.getValue(),
      stage
    });
  };

  return (
    <Paper className={styles.container}>
      <TextField
        ref={x => nameField = x}
        floatingLabelText='Allergy'
        value={item.name}
        onChange={onChange}
        fullWidth={true}
        disabled={disabled}
      />
      <TextField
        ref={x => effectField = x}
        floatingLabelText='Effect'
        value={item.effect}
        onChange={onChange}
        fullWidth={true}
        disabled={disabled}
      />

      <AllergyStage
        stage={item.stage}
        readOnly={false}
        disabled={disabled}
        changeStage={s => {
          stage = s;
          onChange();
        }}
      />

      <div className={styles.actions}>
        <FlatButton
          label='Remove'
          onClick={() => remove(item.id)}
          disabled={disabled}
        />
      </div>
    </Paper>
  );
}