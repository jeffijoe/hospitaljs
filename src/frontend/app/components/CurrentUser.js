import React from 'react';
import styles from 'styles/CurrentUser.scss';
import IconMenu from 'material-ui/lib/menus/icon-menu';
import IconButton from 'material-ui/lib/icon-button';
import MenuItem from 'material-ui/lib/menus/menu-item';
import MoreIcon from 'material-ui/lib/svg-icons/navigation/more-vert';

export default function CurrentUser(props) {
  const {
    firstName,
    lastName,
    hospital,
    role
  } = props.user;

  const { logout } = props;

  const iconButtonElement = (
    <IconButton>
      <MoreIcon color='#fff' />
    </IconButton>
  );

  return (
    <div className={styles.container}>
      <div className={styles.info}>
        <div className={styles.name}>
          {firstName} {lastName}
        </div>
        <div className={styles.hospital}>
          {hospital.name}
        </div>
      </div>

      <IconMenu
        iconButtonElement={iconButtonElement}
        targetOrigin={{horizontal: 'right', vertical: 'top'}}
        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
      >
        <MenuItem value="2" primaryText='Log out' onTouchTap={logout} />
      </IconMenu>
    </div>
  );
}