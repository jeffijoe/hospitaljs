import React from 'react';
import Paper from 'material-ui/lib/paper';
import styles from 'styles/MedicationEditor.scss';
import TextField from 'material-ui/lib/text-field';
import FlatButton from 'material-ui/lib/flat-button';

export default function MedicationEditor ({
  item,
  change,
  remove,
  disabled
}) {
  let nameField, instructionsField;

  const onChange = (e) => {
    change({
      id: item.id,
      name: nameField.getValue(),
      instructions: instructionsField.getValue()
    });
  };

  return (
    <Paper className={styles.container}>
      <TextField
        ref={x => nameField = x}
        floatingLabelText='Medication name'
        value={item.name}
        onChange={onChange}
        fullWidth={true}
        disabled={disabled}
      />
      <TextField
        ref={x => instructionsField = x}
        floatingLabelText='Instructions'
        value={item.instructions}
        onChange={onChange}
        fullWidth={true}
        disabled={disabled}
      />

      <div className={styles.actions}>
        <FlatButton
          label='Remove'
          onClick={() => remove(item.id)}
          disabled={disabled}
        />
      </div>
    </Paper>
  );
}