import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import Body from 'babel!svg-react!../svgs/body.svg';
import styles from 'styles/BodyPartSelector.scss';
import cx from 'classnames';

export default class BodyPartSelector extends Component {
  _initBodyParts() {
    this._prevColors = this._prevColors || {};
    let {
      bodyParts,
      editing,
      saving
    } = this.props;
    bodyParts = bodyParts || [];
    const toRemove = this._prevBodyParts
      ? this._prevBodyParts.filter(x => !bodyParts.some(y => y.bodyPart === x.bodyPart))
      : [];
    const toAdd = this._prevBodyParts
      ? bodyParts.filter(x => !this._prevBodyParts.some(y => y.bodyPart === x.bodyPart))
      : bodyParts;

    const paths = Array.from(findDOMNode(this.refs.body).querySelectorAll('path'));
    paths.forEach(path => {
      if (!this._prevColors[path.id]) {
        this._prevColors[path.id] = path.getAttribute('fill');
      }

      toAdd.forEach(bodyPart => {
        if (bodyPart.bodyPart === path.id) {
          path.style.fill = 'rgb(255, 255, 0)';
        }
      });

      toRemove.forEach(bodyPart => {
        if (bodyPart.bodyPart === path.id) {
          path.style.fill = this._prevColors[path.id];
        }
      });
    });

    this._prevBodyParts = bodyParts;
  }

  componentDidMount () {
    this._initBodyParts();
  }

  componentDidUpdate () {
    this._initBodyParts();
  }

  render() {
    const handleClick = (e) => {
      const target = e.target;
      this.props.toggle(target.id);
    };
    return (
      <div className={cx(styles.body, {[styles.editable]:this.props.editing})}>
        <Body
          ref='body'
          onClick={this.props.editing && handleClick}
        />
      </div>
    );
  }
}