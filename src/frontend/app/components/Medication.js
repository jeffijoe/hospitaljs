import React from 'react';
import styles from 'styles/Medication.scss';
import Paper from 'material-ui/lib/paper';

export default function Medication ({ medication }) {
  return (
    <Paper className={styles.container}>

      <span className={styles.name}>
        {medication.name}
      </span>

      <span className={styles.instructions}>
        &nbsp;
        &mdash;
        &nbsp;
        {medication.instructions}
      </span>
    </Paper>
  );
}