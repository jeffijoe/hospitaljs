import React from 'react';
import Dialog from 'material-ui/lib/dialog';
import FlatButton from 'material-ui/lib/flat-button';
import TextField from 'material-ui/lib/text-field';


export default function ApprovalRequestRejectionDialog ({
  showing,
  cancel,
  rejectionMessage,
  changeRejectionMessage,
  reject,
  error
}) {
  let input;
  const onReasonChange = () => {
    changeRejectionMessage(input.getValue());
  };

  const actions = [
    <FlatButton
      label='Cancel'
      secondary={true}
      onTouchTap={cancel}
    />,
    <FlatButton
      label='Reject'
      primary={true}
      onTouchTap={reject}
    />
  ];
  return (
    <div>
      <Dialog
        title='Rejection'
        modal={false}
        open={showing}
        onRequestClose={cancel}
        actions={actions}
      >
        <TextField
          fullWidth={true}
          multiLine={true}
          value={rejectionMessage}
          errorText={error}
          placeholder='Please specify a rejection reason..'
          ref={x => input = x}
          onChange={onReasonChange}
        />
      </Dialog>
    </div>
  );
}