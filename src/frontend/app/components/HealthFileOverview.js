import React from 'react';
import styles from 'styles/HealthFileOverview.scss';
import Heading from './Heading';
import Allergy from './Allergy';
import Medication from './Medication';
import RaisedButton from 'material-ui/lib/raised-button';
import FloatingActionButton from 'material-ui/lib/floating-action-button';
import ModeEdit from 'material-ui/lib/svg-icons/editor/mode-edit';
import PatientRecordListItem from './PatientRecordListItem';
import GenericLoader from './GenericLoader';
import {Link} from 'react-router';

const Section = ({name, children}) => {
  return (
    <div>
      <Heading thin={true}>{name}</Heading>
      <div>
        {children}
      </div>
    </div>
  );
};

const PatientRecordsList = ({ records, view }) => (
  <div>
    {records.map(record =>
      <PatientRecordListItem
        key={record.id}
        record={record}
        view={view}
      />
    )}
  </div>
);

const NoRecords = () => (
  <div className={styles.noRecords}>
    No patient records in this health file... yet!
  </div>
);

export default function HealthFileOverview (props) {
  const {
    healthFile,
    changeStage,
    patientRecords,
    edit,
    viewRecord,
    newPatientRecord,
    addButtonVisible,
    editButtonVisible
  } = props;

  return (
    <div className={styles.container}>

      <Section name='Allergies'>
        {healthFile.allergies.length === 0 &&
          <div>
            No allergies.
          </div>
        }
        {healthFile.allergies.map(allergy =>
          <Allergy key={allergy.id} allergy={allergy} />
        )}
      </Section>
      <Section name='Medication'>
        {healthFile.medication.length === 0 &&
          <div>
            No medication.
          </div>
        }
        {healthFile.medication.map(m =>
          <Medication key={m.id} medication={m} />
        )}
      </Section>
      {editButtonVisible &&
        <div style={{textAlign: 'right', paddingTop: 20}}>
          <FloatingActionButton
            id='editHealthfile'
            primary={true}
            onClick={edit}
          >
            <ModeEdit/>
          </FloatingActionButton>
        </div>
      }
      <Section name='Records'>
          {patientRecords.loading &&
            <GenericLoader text='Loading records'/>
          }
          {!patientRecords.loading &&
            <div>
              {addButtonVisible &&
                <RaisedButton
                  id='addrecord'
                  label='Add new record'
                  secondary={true}
                  onClick={() => newPatientRecord(healthFile.id)}
                />
              }
              <div className={styles.recordsContainer}>
                {patientRecords.hasRecords
                  ? <PatientRecordsList
                      records={patientRecords.records}
                      view={viewRecord}
                    />
                  : <NoRecords/>
                }
              </div>
            </div>
          }
      </Section>
    </div>
  );
}