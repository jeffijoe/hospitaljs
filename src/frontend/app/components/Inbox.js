import React from 'react';
import styles from 'styles/Inbox.scss';
import Heading from './Heading';
import ApprovalRequest from './ApprovalRequest';
import FlatButton from 'material-ui/lib/flat-button';
import ApprovalRequestVisibilityFilters from './ApprovalRequestVisibilityFilters';
import {VisibilityFilter} from 'actions/approvalRequestActions';
import LargeError from './LargeError';
import CSSTransitionGroup from 'react-addons-css-transition-group';

const NoRequests = () => (
  <div className={styles.noRequests}>
    No approval requests.. yet!
  </div>
);

export default function Inbox (props) {
  const {
    approvalRequests,
    onChangeVisibilityFilter,
    view,
    approve,
    reject
  } = props;
  const {
    visibilityFilter,
    filters
  } = approvalRequests;

  const showError = !approvalRequests.loading && approvalRequests.success === false;
  const showRequests = !approvalRequests.loading && approvalRequests.success !== false;
  const hasRequests = (approvalRequests.approvalRequests && approvalRequests.approvalRequests.length > 0);

  return (
    <div className={styles.container}>
      <Heading style={{ margin: 0 }}>Inbox</Heading>
      <div style={{ textAlign: 'right', paddingBottom: 10 }}>
        <ApprovalRequestVisibilityFilters
          onChangeVisibilityFilter={onChangeVisibilityFilter}
          current={visibilityFilter}
          filters={filters}
        />
      </div>
      {showError &&
        <LargeError message='Something bad happened! :('/>
      }

      {showRequests &&
        <div>
          {hasRequests
            ? (
              <div>
                <CSSTransitionGroup
                  transitionName='approvalRequestTransition'
                  transitionEnterTimeout={600}
                  transitionLeaveTimeout={500}
                >
                  {approvalRequests.approvalRequests.map(request =>
                    <ApprovalRequest
                      key={request.id}
                      request={request}
                      view={view}
                      approve={approve}
                      reject={reject}
                    />
                  )}
                </CSSTransitionGroup>
              </div>
            )
            : <NoRequests />
          }
        </div>
      }
    </div>
  );
}