import React from 'react';
import Heading from './Heading';

export default function PatientRecordSection ({ title, children }) {
  return (
    <div>
      <Heading>{title}</Heading>
      {children}
    </div>
  );
}