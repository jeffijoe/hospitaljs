import React from 'react';
import styles from 'styles/PatientInfo.scss';
import formatDate from 'util/formatDate';
import Paper from 'material-ui/lib/paper';

const Info = ({ label, children }) => (
  <div className={styles.infoContainer}>
    <div className={styles.infoLabel}>
      {label}
    </div>
    <div className={styles.info}>
      {children}
    </div>
  </div>
);

export default function PatientInfo (props) {
  const { patient } = props;
  return (
    <div className={styles.container}>
      <Paper className={styles.main}>
        <h1 className={styles.patientName}>
          {patient.firstName} {patient.lastName}
        </h1>
        <div className={styles.dob}>
          {formatDate(patient.dateOfBirth, 'LL')}
          {' '}
          <span className={styles.dobLabel}>(DOB)</span>

        </div>
        <Info label='Guardian'>
          <div>
            {patient.guardian.firstName} {patient.guardian.lastName}
          </div>
          <div>
            {patient.guardian.phoneNumber}
          </div>
        </Info>

        <Info label='Address'>
          <div>
            {patient.address.street}
          </div>
          <div>
            {patient.address.zip} {patient.address.city}, {patient.address.country}
          </div>
        </Info>

      </Paper>
    </div>
  );
}