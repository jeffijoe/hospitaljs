import React from 'react';
import Heading from './Heading';
import styles from 'styles/LargeError.scss';
import ErrorIcon from 'material-ui/lib/svg-icons/alert/error';
import Colors from 'material-ui/lib/styles/colors';

const ICON_SIZE = 64;

export default function LargeError ({ message }) {
  return (
    <div className={styles.container}>
      <ErrorIcon
        color={Colors.red500}
        style={{
          width: ICON_SIZE,
          height: ICON_SIZE
        }}
      />
      <Heading>An error occured!</Heading>
      <div className={styles.message}>
        {message}
      </div>
    </div>
  );
}