import React, { Component } from 'react';
import { connect } from 'react-redux';
import HealthFileLoading from 'components/HealthFileLoading';
import Container from 'components/Container';
import Row from 'components/Row';
import Column from 'components/Column';
import PatientInfo from 'components/PatientInfo';
import HealthFileError from 'components/HealthFileError';
import NoHealthFile from 'components/NoHealthFile';
import healthFilePageSelector from 'selectors/healthFilePageSelector';
import { fetchHealthFile, createHealthFile, editHealthFile } from 'actions/healthFileActions';
import { showPatientRecord, showNewPatientRecord } from 'actions/patientRecordActions';
import { setBackButtonAction } from 'actions/headerActions';
import HealthFileOverview from 'components/HealthFileOverview';
import patientRecordsSelector from 'selectors/patientRecordsSelector';

/*eslint-disable no-shadow*/

class HealthFilePage extends Component {
  componentWillMount() {
    this.props.setBackButtonAction(null);
    this.props.fetchHealthFile(this.props.params.id);
  }

  render() {
    const {
      healthFilePage,
      patientRecords,
      editHealthFile,
      showNewPatientRecord,
      addButtonVisible,
      editButtonVisible
    } = this.props;

    return (
      <div style={{ marginTop: 20 }}>
        {healthFilePage.loading &&
          <HealthFileLoading />
        }
        {healthFilePage.success === false &&
          <HealthFileError />
        }
        {!healthFilePage.loading && healthFilePage.success &&
          <Row pad={true} responsive={true}>
            <Column flex='1 1 auto'>
              <PatientInfo patient={healthFilePage.personInfo} />
            </Column>
            <Column flex='6 1 auto'>
              {!healthFilePage.hasHealthFile && healthFilePage.personInfo &&
                <NoHealthFile
                  ssn={healthFilePage.personInfo.ssn}
                  personFirstName={healthFilePage.personInfo.firstName}
                  personLastName={healthFilePage.personInfo.lastName}
                  create={this.props.createHealthFile}
                />
              }
              {healthFilePage.hasHealthFile &&
                <HealthFileOverview
                  healthFile={healthFilePage.healthFile}
                  patientRecords={patientRecords}
                  newPatientRecord={showNewPatientRecord}
                  edit={() => editHealthFile(healthFilePage.healthFile.id)}
                  viewRecord={this.props.showPatientRecord}
                  addButtonVisible={addButtonVisible}
                  editButtonVisible={editButtonVisible}
                />
              }
            </Column>
          </Row>
        }
      </div>
    );
  }
}

const mapStateToProps = healthFilePageSelector;

export default connect(
  mapStateToProps,
  {
    fetchHealthFile,
    createHealthFile,
    editHealthFile,
    showPatientRecord,
    showNewPatientRecord,
    setBackButtonAction
  }
)(HealthFilePage);