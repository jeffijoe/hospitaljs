import React, { Component, PropTypes } from 'react';
import { Provider } from 'react-redux';
import AppRoutes from '../../routes';

export default class Root extends Component {
  render() {
    const { store } = this.props;
    return (
      <Provider store={store}>
        <AppRoutes getState={store.getState} />
      </Provider>
    );
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired
};