import React, { Component, PropTypes } from 'react';
import { Provider } from 'react-redux';
import AppRoutes from '../../routes';
import DevTools from './DevTools';

export default class Root extends Component {
  render() {
    const { store } = this.props;
    return (
      <Provider store={store}>
        <div>
          <AppRoutes getState={store.getState} />
          <DevTools />
        </div>
      </Provider>
    );
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired
};