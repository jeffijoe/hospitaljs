import { connect } from 'react-redux';
import React, { Component } from 'react';
import LargeError from 'components/LargeError';

class UnauthorizedPage extends Component {
  render() {
    const message = `You don't have permission to go here! Shoo!`;
    return (
      <div style={{ textAlign: 'center', paddingTop: 20 }}>
        <LargeError message={message}/>
      </div>
    );
  }
}

export default connect(
)(UnauthorizedPage);