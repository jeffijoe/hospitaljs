import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoginForm from 'components/LoginForm';
import { login } from 'actions/loginActions';
import { setBackButtonAction } from 'actions/headerActions';


class Login extends Component {
  componentWillMount() {
    this.props.setBackButtonAction(null);
  }

  render() {
    return (
      <div>
        <LoginForm
          onSubmit={this.props.login}
          isLoading={this.props.loading}
          success={this.props.success}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state.login
});

export default connect(
  mapStateToProps,
  {
    login,
    setBackButtonAction
  }
)(Login);