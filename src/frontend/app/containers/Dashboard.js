/*eslint-disable no-shadow*/
import React, { Component } from 'react';
import Paper from 'material-ui/lib/paper';
import Container from 'components/Container';
import Row from 'components/Row';
import Column from 'components/Column';
import Inbox from 'components/Inbox';
import HealthFileLookup from 'components/HealthFileLookup';
import { connect } from 'react-redux';
import { login } from 'actions/loginActions';
import { openHealthFile, changeSsn } from 'actions/healthFileLookupActions';
import {
  fetchApprovalRequests,
  setVisibilityFilter,
  showRejectionDialog,
  changeRejectionMessage,
  approveApprovalRequest,
  rejectApprovalRequest,
  cancelRejectionDialog
} from 'actions/approvalRequestActions';
import { showPatientRecord } from 'actions/patientRecordActions';
import { setBackButtonAction } from 'actions/headerActions';
import ApprovalRequestRejectionDialog from 'components/ApprovalRequestRejectionDialog';
import dashboardSelector from 'selectors/dashboardSelector';

class Dashboard extends Component {
  componentWillMount() {
    this.props.setBackButtonAction(null);
    if (this.props.inboxVisible) {
      this.props.fetchApprovalRequests();
    }
  }

  render() {
    const {
      approvalRequests,
      healthFileLookup,
      openHealthFile,
      setVisibilityFilter,
      showPatientRecord,
      inboxVisible,
      changeRejectionMessage,
      showRejectionDialog,
      approveApprovalRequest,
      rejectApprovalRequest,
      cancelRejectionDialog
    } = this.props;

    return (
      <Container>
        <HealthFileLookup
          healthFileLookup={healthFileLookup}
          onChangeSsn={this.props.changeSsn}
          onOpen={openHealthFile}
        />
        {inboxVisible &&
          <div>
            <Inbox
              approvalRequests={approvalRequests}
              onChangeVisibilityFilter={setVisibilityFilter}
              view={showPatientRecord}
              approve={approveApprovalRequest}
              reject={showRejectionDialog}
            />

            <ApprovalRequestRejectionDialog
              error={approvalRequests.rejectionErrorMessage}
              showing={approvalRequests.showRejectionDialog}
              cancel={cancelRejectionDialog}
              rejectionMessage={approvalRequests.rejectionMessage}
              changeRejectionMessage={changeRejectionMessage}
              reject={rejectApprovalRequest}
            />
          </div>
        }
      </Container>
    );
  }
}

const mapStateToProps = dashboardSelector;

export default connect(
  mapStateToProps,
  {
    openHealthFile,
    changeSsn,
    fetchApprovalRequests,
    setVisibilityFilter,
    showPatientRecord,
    setBackButtonAction,
    showRejectionDialog,
    changeRejectionMessage,
    approveApprovalRequest,
    rejectApprovalRequest,
    cancelRejectionDialog
  }
)(Dashboard);