import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  fetchHealthFile,
  addNewMedication,
  updateMedication,
  removeMedication,
  addNewAllergy,
  updateAllergy,
  removeAllergy,
  saveHealthFile
} from 'actions/healthFileEditorActions';
import { setBackButtonAction } from 'actions/headerActions';
import {routeActions} from 'redux-simple-router';
import Container from 'components/Container';
import Row from 'components/Row';
import Column from 'components/Column';
import GenericLoader from 'components/GenericLoader';
import healthFileEditorSelector from 'selectors/healthFileEditorSelector';
import ItemsEditor from 'components/ItemsEditor';
import MedicationEditor from 'components/MedicationEditor';
import AllergyEditor from 'components/AllergyEditor';
import FloatingActionButton from 'material-ui/lib/floating-action-button';
import SaveIcon from 'material-ui/lib/svg-icons/content/save';
import LinearProgress from 'material-ui/lib/linear-progress';

class HealthFileEditorPage extends Component {
  componentWillMount() {
    const healthFileId = this.props.params.id;
    this.props.setBackButtonAction(routeActions.push(`/healthfile/${healthFileId}`));
    this.props.fetchHealthFile(healthFileId);
  }

  render() {
    const props = this.props;
    return (
      <Container>
        <Row responsive={true}>
          <Column>
            {props.loading &&
              <GenericLoader text='Loading health file' />
            }
            {!props.loading && props.success !== false &&
              <div>
                <ItemsEditor
                  title='Allergies'
                  id='allergies-editor'
                  items={props.allergies}
                  itemEditor={AllergyEditor}
                  add={this.props.addNewAllergy}
                  change={this.props.updateAllergy}
                  remove={this.props.removeAllergy}
                  disabled={props.saving}
                />

                <ItemsEditor
                  id='medication-editor'
                  title='Medication'
                  items={props.medication}
                  itemEditor={MedicationEditor}
                  add={this.props.addNewMedication}
                  change={this.props.updateMedication}
                  remove={this.props.removeMedication}
                  disabled={props.saving}
                />

                {props.saving &&
                  <div style={{paddingTop: 10}}>
                      <LinearProgress mode='indeterminate'/>
                  </div>
                }
                {!props.saving &&
                  <div style={{textAlign: 'right', paddingTop: 20}}>
                    <FloatingActionButton
                      id='saveHealthFile'
                      onClick={this.props.saveHealthFile}
                    >
                      <SaveIcon/>
                    </FloatingActionButton>
                  </div>
                }

              </div>

            }
          </Column>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = healthFileEditorSelector;

const mapDispatchToProps = {
  fetchHealthFile,
  addNewMedication,
  updateMedication,
  removeMedication,
  addNewAllergy,
  updateAllergy,
  removeAllergy,
  saveHealthFile,
  setBackButtonAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HealthFileEditorPage);