import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  newPatientRecord,
  createPatientRecord,
  fetchPatientRecord,
  editPatientRecord,
  savePatientRecord,
  cancelEditingPatientRecord,
  changePatientRecord,
  addProcedure,
  changeProcedure,
  removeProcedure,
  addMedia,
  removeMedia,
  toggleBodyPart
} from 'actions/patientRecordActions';
import {setBackButtonAction} from 'actions/headerActions';
import Container from 'components/Container';
import Row from 'components/Row';
import Column from 'components/Column';
import PatientRecordSection from 'components/PatientRecordSection';
import PaperTextField from 'components/PaperTextField';
import PatientRecordPageActions from 'components/PatientRecordPageActions';
import patientRecordPageSelector from 'selectors/patientRecordPageSelector';
import GenericLoader from 'components/GenericLoader';
import PatientRecordProcedures from 'components/PatientRecordProcedures';
import LargeError from 'components/LargeError';
import PatientRecordMediaList from 'components/PatientRecordMediaList';
import BodyPartSelector from 'components/BodyPartSelector';

/*eslint-disable no-shadow*/

const TextSection = ({
  title,
  value,
  prop,
  change,
  editing,
  saving,
  error
}) => {
  if (!value && !editing) return <div></div>;
  const changePatientRecord = (newValue) => change({ [prop]: newValue });
  return (
    <PatientRecordSection title={title}>
      <PaperTextField
        value={value}
        editing={editing}
        error={error}
        saving={saving}
        change={changePatientRecord}
      />
    </PatientRecordSection>
  );
};

class PatientRecordPage extends Component {
  componentWillMount() {
    if (this.props.params.healthFileId) {
      this.props.newPatientRecord(this.props.params.healthFileId);
    } else {
      this.props.fetchPatientRecord(this.props.params.id);
    }
  }

  render() {
    const {
      success,
      editing,
      healthFileId,
      editPatientRecord,
      createPatientRecord,
      saving,
      loading,
      record,
      savePatientRecord,
      cancelEditingPatientRecord,
      changePatientRecord,
      hasAnyProcedures,
      hasAnyMedia,
      addProcedure,
      removeProcedure,
      changeProcedure,
      addMedia,
      removeMedia,
      isNew,
      editButtonVisible,
      toggleBodyPart,
      validationErrors
    } = this.props;

    return (
      <Container>
        <Row responsive={true}>
          <Column>
            {loading &&
              <GenericLoader text='Loading record..' />
            }
            {!loading && success !== false &&
              <div>
                <TextSection
                  title='Symptoms'
                  value={record.symptoms}
                  error={validationErrors.symptoms}
                  prop='symptoms'
                  editing={editing}
                  saving={saving}
                  change={changePatientRecord}
                />

                <TextSection
                  title='Diagnosis'
                  value={record.diagnosis}
                  error={validationErrors.diagnosis}
                  prop='diagnosis'
                  editing={editing}
                  saving={saving}
                  change={changePatientRecord}
                />

                <TextSection
                  title='Medication'
                  value={record.medication}
                  error={validationErrors.medication}
                  prop='medication'
                  editing={editing}
                  saving={saving}
                  change={changePatientRecord}
                />

                <TextSection
                  title='Lab Results'
                  value={record.labResults}
                  error={validationErrors.labResults}
                  prop='labResults'
                  editing={editing}
                  saving={saving}
                  change={changePatientRecord}
                />

                {(hasAnyProcedures || editing) &&
                  <PatientRecordSection title='Procedures'>
                    <PatientRecordProcedures
                      editing={editing}
                      saving={saving}
                      procedures={record.procedures}
                      add={addProcedure}
                      change={changeProcedure}
                      remove={removeProcedure}
                    />
                  </PatientRecordSection>
                }

                {(hasAnyMedia || editing) &&
                  <PatientRecordSection title='Media'>
                    <PatientRecordMediaList
                      editing={editing}
                      saving={saving}
                      media={record.media}
                      add={addMedia}
                      remove={removeMedia}
                    />

                  </PatientRecordSection>
                }

                <PatientRecordSection title='Relevant body parts'>
                  <BodyPartSelector
                    bodyParts={record.bodyParts}
                    editing={editing}
                    saving={saving}
                    toggle={toggleBodyPart}
                  />
                </PatientRecordSection>

                {editButtonVisible &&
                  <PatientRecordPageActions
                    editing={editing}
                    edit={editPatientRecord}
                    saving={saving}
                    save={isNew ? () => createPatientRecord(healthFileId) : savePatientRecord}
                    cancel={cancelEditingPatientRecord}
                  />
                }
              </div>
            }

            {success === false &&
              <LargeError message='Could not load that record.'/>
            }
          </Column>
        </Row>
      </Container>
    );
  }
}


const mapStateToProps = patientRecordPageSelector;

export default connect(
  mapStateToProps,
  {
    newPatientRecord,
    fetchPatientRecord,
    createPatientRecord,
    editPatientRecord,
    savePatientRecord,
    cancelEditingPatientRecord,
    changePatientRecord,
    addProcedure,
    changeProcedure,
    removeProcedure,
    addMedia,
    removeMedia,
    setBackButtonAction,
    toggleBodyPart
  }
)(PatientRecordPage);