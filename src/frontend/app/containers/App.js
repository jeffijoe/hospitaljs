import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from 'components/Header';
import Footer from 'components/footer';
import styles from 'styles/App.scss';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { logout as logoutAction } from 'actions/loginActions';
import { routeActions } from 'redux-simple-router';
import Snackbar from 'material-ui/lib/snackbar';
import {hideNotification} from 'actions/notificationActions';
import {triggerBackButtonAction, setBackButtonAction} from 'actions/headerActions';

const toIndex = () => routeActions.push('/');

injectTapEventPlugin();

/*eslint-disable no-shadow*/

/**
 * The App container.
 */
class App extends Component {


  render() {
    const {
      currentUser,
      logout,
      notifications,
      hideNotification,
      header,
      triggerBackButtonAction
    } = this.props;

    let triggerBackButton;
    if (header.backButtonAction) {
      triggerBackButton = triggerBackButtonAction;
    }

    return (
      <div>
        <Header
          currentUser={currentUser}
          logout={logout}
          onTitleClick={this.props.toIndex}
          onBackClick={triggerBackButton}
        />
        <div>
          {this.props.children}
        </div>

        <Footer year={new Date().getFullYear()} />

        <Snackbar
          open={notifications.open}
          message={notifications.message}
          onRequestClose={hideNotification}
        />
      </div>
    );
  }
}

export default connect(
  x => x,
  {
    logout: logoutAction,
    toIndex,
    hideNotification,
    triggerBackButtonAction
  }
)(App);