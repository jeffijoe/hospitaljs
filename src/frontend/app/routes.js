import React from 'react';
import Router, { Route, IndexRoute, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import requireAuthFn from 'util/requireAuth';
import permissions from 'util/permissions';
import Dashboard from './containers/Dashboard';
import Login from './containers/Login';
import App from './containers/App';
import HealthFilePage from './containers/HealthFilePage';
import HealthFileEditorPage from './containers/HealthFileEditorPage';
import PatientRecordPage from './containers/PatientRecordPage';
import UnauthorizedPage from './containers/UnauthorizedPage';

function AppRoutes({ getState }) {
  const requireAuth = requireAuthFn.bind(null, getState);
  return (
    <Router history={browserHistory}>
      <Route path='/' component={App}>
        <IndexRoute
          component={Dashboard}
          onEnter={requireAuth(permissions.readHealthfile)}
        />
        <Route
          path='/login'
          component={Login}
        />
        <Route
          path='/healthfile/:id'
          component={HealthFilePage}
          onEnter={requireAuth(permissions.readHealthfile)}
        />
        <Route
          path='/healthfile/:id/edit'
          component={HealthFileEditorPage}
          onEnter={requireAuth(permissions.readHealthfile | permissions.writeHealthfile)}
        />
        <Route
          path='/record/:id'
          component={PatientRecordPage}
          onEnter={requireAuth(permissions.readHealthfile)}
        />
        <Route
          path='/healthfile/:healthFileId/new-record'
          component={PatientRecordPage}
          onEnter={requireAuth(permissions.readHealthfile | permissions.writeHealthfile)}
        />
        <Route
          path='/unauthorized'
          component={UnauthorizedPage}
        />
      </Route>
    </Router>
  );
}

export default AppRoutes;