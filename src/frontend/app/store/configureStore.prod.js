import { createStore, applyMiddleware, compose } from 'redux';
import createHistory from 'history/lib/createBrowserHistory';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import { syncHistory } from 'redux-simple-router';
import { browserHistory } from 'react-router';

const router = syncHistory(browserHistory);

const finalCreateStore = compose(
  applyMiddleware(
    thunk,
    router
  ),
)(createStore);

export default function configureStore(initialState) {
  const store = finalCreateStore(rootReducer, initialState);
  router.listenForReplays(store);
  return store;
}