//import { processModel } from './modelUtil';
import { find, propEq } from 'ramda';
import cache from 'memory-cache';

const delay = ms => new Promise(r => setTimeout(r, ms));

const people = [
  {
    'ssn': 'DK0102902894',
    'firstName': 'Jeff',
    'lastName': 'Hansen',
    'dateOfBirth': '1994-09-06',
    'phoneNumber': '+45 12345678',
    'guardian': {
      firstName: 'Amanda',
      lastName: 'Callesen',
      phoneNumber: '+45 12345689'
    },
    'address': {
      'street': 'Damgade 80',
      'zip': '6400',
      'city': 'Sønderborg',
      'country': 'Denmark'
    }
  },
  {
    'ssn': 'DK0102875421',
    'firstName': 'Mads',
    'lastName': 'Falkenstrøm',
    'dateOfBirth': '1993-06-21',
    'phoneNumber': '+45 87654321',
    'guardian': {
      firstName: 'Ekaterina',
      lastName: 'Zhdanova',
      phoneNumber: '+45 97654321'
    },
    'address': {
      'street': 'Amagervej 21',
      'zip': '2600',
      'city': 'Amager',
      'country': 'Denmark'
    }
  },
  {
    'ssn': 'US721074426',
    'firstName': 'Clinton',
    'lastName': 'Lester Booker',
    'dateOfBirth': '1911-02-13',
    'phoneNumber': '+1 555 123 123 5442',
    'guardian': {
      firstName: 'Marie',
      lastName: 'Booker',
      'phoneNumber': '+1 555 123 123 5442'
    },
    'address': {
      'street': 'Railroad Way',
      'zip': '1234',
      'city': 'New York',
      'country': 'United States of America'
    }
  }
];

/**
 * Gets the civilian person
 * @param  {String} ssn The search term.
 * @return {Object} The found person Object.
 */
async function getCivilian (ssn) {
  let personInfo = {};
  const inCache = cache.get(ssn);
  if (inCache) {
    personInfo = inCache;
  } else {
    await delay(500);
    personInfo = find(propEq('ssn', ssn))(people);
    if (!personInfo) return null;
    cache.put(personInfo.ssn, personInfo, 86400000);
  }

  return { personInfo };
}

export default {
  getCivilian
};