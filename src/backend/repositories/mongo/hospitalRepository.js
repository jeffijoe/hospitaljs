import Hospital from './models/Hospital';
import { processModel } from './modelUtil';
import { compose, omit, map } from 'ramda';

async function createHospital (hospital) {
  const hospitalModel = new Hospital(hospital);
  await hospitalModel.save();
  return processModel(hospitalModel);
}

export default {
  createHospital
};