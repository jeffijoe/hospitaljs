import Role from './models/Role';
import { processModel } from './modelUtil';
import { compose, omit, map } from 'ramda';

async function createRole (role) {
  const roleModel = new Role(role);
  await roleModel.save();
  return processModel(roleModel);
}

export default {
  createRole
};