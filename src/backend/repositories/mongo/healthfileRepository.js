import Healthfile from './models/Healthfile';
import mongoose from 'mongoose';
import { processModel, removeProps, removeArrayProps } from './modelUtil';
import { compose, omit, map } from 'ramda';

const mapHealthfile = compose(
   x => ({
     ...x,
     allergies: x.allergies ? removeArrayProps(x.allergies) : [],
     medication: x.medication ? removeArrayProps(x.medication) : [],
     ownerHospital: removeProps(x.ownerHospital)
   }),
  processModel
);

const createSsnOrIdQuery = ssnOrId => {
  return {
    $or: [
      { ssn: ssnOrId },
      mongoose.Types.ObjectId.isValid(ssnOrId) ? {_id: new mongoose.Types.ObjectId(ssnOrId)} : null
    ].filter(x => x !== null)
  };
};

/**
 * Gets all the health files.
 * @param  {String} ssn The search term.
 * @return {Object} The health file that was found.
 */
async function getHealthfile (ssn) {
  const healthfile = await Healthfile.findOne(createSsnOrIdQuery(ssn)).populate('ownerHospital');
  if (!healthfile) return null;
  return mapHealthfile(healthfile);
}

/**
 * Creates a new health file.
 * @param  {Object} healthfile The health file object.
 * @return {Object} The health file that was saved.
 */
async function createHealthfile (healthfile) {
  const healthfileModel = new Healthfile(healthfile);
  await healthfileModel.save();
  return processModel(healthfileModel);
}

async function updateHealthfile (ssn, healthfile) {
  const newhealthfile = await Healthfile.findOneAndUpdate(
    createSsnOrIdQuery(ssn),
    healthfile,
    { new: true }
  ).populate('ownerHospital');
  return mapHealthfile(newhealthfile);
}

export default {
  createHealthfile,
  getHealthfile,
  updateHealthfile
};