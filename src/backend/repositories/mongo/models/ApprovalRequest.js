import mongoose from 'mongoose';
export default mongoose.model('approvalrequest', mongoose.Schema({
  requestedAt: { type: Date, default: Date.now },
  requester: {
    type: mongoose.Schema.ObjectId,
    ref: 'user'
  },
  status: { type: String, enum: ['pending', 'approved', 'rejected'], default: 'pending' },
  rejectedReason: String,
  patientRecord: {
    type: mongoose.Schema.ObjectId,
    ref: 'patientRecord'
  },
  hospital: {
    type: mongoose.Schema.ObjectId,
    ref: 'hospital'
  }
}));