import mongoose from 'mongoose';
export default mongoose.model('role', mongoose.Schema({
  name: String,
  permissions: Number
}));