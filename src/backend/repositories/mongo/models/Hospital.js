import mongoose from 'mongoose';
export default mongoose.model('hospital', mongoose.Schema({
  name: String,
  location: {
    street: String,
    city: String,
    zip: String,
    country: String
  }
}));