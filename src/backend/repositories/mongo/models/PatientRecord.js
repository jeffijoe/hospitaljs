import mongoose from 'mongoose';
export default mongoose.model('patientRecord', mongoose.Schema({
  medication: String,
  symptoms: String,
  diagnosis: String,
  dateStart: { type: Date, default: Date.now },
  dateEnd: Date,
  dateModified: { type: Date, default: Date.now },
  labResults: String,
  bodyParts: [{
    bodyPart: String
  }],
  procedures: [{
    step: String,
    dateTime: { type: Date, default: Date.now }
  }],
  media: [{
    mediaType: String,
    fileName: String,
    fileSize: String,
    fileUrl: String
  }],
  healthfileId: mongoose.Schema.ObjectId
}));