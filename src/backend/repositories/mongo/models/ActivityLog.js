import mongoose from 'mongoose';
export default mongoose.model('activitylog', mongoose.Schema({
  type: String,
  eventTime: { type: Date, default: Date.now },
  user: {
    employeeId: String,
    firstName: String,
    lastName: String
  },
  healthfile: {
    ssn: String,
    id: mongoose.Schema.ObjectId
  }
}));