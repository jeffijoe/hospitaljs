import mongoose from 'mongoose';
import Hospital from './Hospital';
import Role from './Role';

export default mongoose.model('user', mongoose.Schema({
  firstName: String,
  lastName: String,
  department: String,
  employeeId: String,
  phoneNumber: String,
  email: String,
  password: String,
  role: {
    type: mongoose.Schema.ObjectId,
    ref: 'role'
  },
  hospital: {
    type: mongoose.Schema.ObjectId,
    ref: 'hospital'
  }
}));