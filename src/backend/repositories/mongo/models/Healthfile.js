import mongoose from 'mongoose';
export default mongoose.model('healthfile', mongoose.Schema({
  ssn: String,
  allergies: [{
    name: String,
    stage: Number,
    effect: String
  }],
  medication: [{
    name: String,
    instructions: String
  }],
  ownerHospital: {
    type: mongoose.Schema.ObjectId,
    ref: 'hospital'
  }
}));