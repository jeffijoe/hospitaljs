import mongoose from 'mongoose';
export default mongoose.model('notes', mongoose.Schema({
  message: String,
  createdAt: { type: Date, default: Date.now },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'user'
  },
  patientRecord: {
    type: mongoose.Schema.ObjectId,
    ref: 'patientRecord'
  }
}));