import PatientRecord from './models/PatientRecord';
import { processModel, removeProps, removeArrayProps } from './modelUtil';
import { compose, map, merge } from 'ramda';

const mapPatientRecord = compose(
   x => ({
     ...x,
     media: x.media ? removeArrayProps(x.media) : [],
     procedures: x.procedures ? removeArrayProps(x.procedures) : [],
     bodyParts: x.bodyParts ? removeArrayProps(x.bodyParts) : []
   }),
  processModel
);

async function getPatientRecords (id) {
  const patientrecords = await PatientRecord.find({ 'healthfileId': id });
  if (!patientrecords) return null;
  return map(mapPatientRecord, patientrecords);
}

async function getPatientRecord (id) {
  const patientrecord = await PatientRecord.findById(id);
  if (!patientrecord) return null;
  return mapPatientRecord(patientrecord);
}

async function createPatientRecord (healthfileId, patientrecord) {
  patientrecord = merge(patientrecord, {healthfileId: healthfileId});
  const patientRecordModel = new PatientRecord(patientrecord);
  await patientRecordModel.save();
  return mapPatientRecord(patientRecordModel);
}

async function updatePatientRecord (id, patientrecord) {
  const newPatientRecord = await PatientRecord.findByIdAndUpdate(id, patientrecord, {new: true});
  return mapPatientRecord(newPatientRecord);
}

export default {
  getPatientRecords,
  getPatientRecord,
  createPatientRecord,
  updatePatientRecord
};