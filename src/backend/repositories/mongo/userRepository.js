import User from './models/User';
import {hash, compare} from '../../util/bcrypt';
import { compose, omit, map } from 'ramda';
import { processModel, removeProps } from './modelUtil';
import mongoose from 'mongoose';

const mapUser = compose(
  x => ({
    ...x,
    role: removeProps(x.role),
    hospital: removeProps(x.hospital)
  }),
  omit('password'),
  processModel
);

/**
 * Gets the user by credentials.
 * @param  {String} credentials.employeeId The Employee ID of the user to fetch.
 * @param  {String} credentials.password The unhashed password of the user to fetch.
 * @return {Promise<Object>} The user.
 */
async function getUserByCredentials (credentials) {
  const user = await User.findOne({
    employeeId: new RegExp(credentials.employeeId, 'i')
  }).populate('hospital role');

  if (!user) return null;
  const passwordIsGood = await compare(credentials.password, user.password);
  if (!passwordIsGood) return null;
  return mapUser(user);
}

/**
 * Creates a new user.
 * @param  {Object} user The user object.
 * @return {Object}      The user that was saved.
 */
async function createUser (user) {
  const userModel = new User({
    ...user,
    password: await hash(user.password)
  });

  await userModel.save();
  return mapUser(userModel);
}

async function getUserById (id) {
  const user = await User.findById(id).populate('hospital role');
  if (!user) return null;
  return mapUser(user);
}

export default {
  getUserByCredentials,
  getUserById,
  createUser
};