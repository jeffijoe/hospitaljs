import {compose, map, curry} from 'ramda';

/**
 * Calls toObject on a model.
 * @param  {Model} x The model.
 * @return {Object}   The object.
 */
export const toObject = x => x ? x.toObject() : x;

/**
 * Removes the __v and _id, and assigns the ID property instead.
 * @param  {Object} x The object to reassign things on.
 * @return {Object}   The new object.
 */
export const removeProps = x => {
  if (!x) return x;
  delete x.__v;
  x.id = x._id;
  delete x._id;
  return x;
};

export const removeArrayProps = x => map(removeProps, x);

/**
 * Composition of toObject and removeProps
 */
export const processModel = compose(
  removeProps,
  toObject
);