import mongoose from 'mongoose';
import userRepository from './userRepository';
import roleRepository from './roleRepository';
import hospitalRepository from './hospitalRepository';
import healthfileRepository from './healthfileRepository';
import activityLogRepository from './activityLogRepository';
import patientRecordRepository from './patientRecordRepository';
import approvalRequestRepository from './approvalRequestRepository';
import noteRepository from './noteRepository';

export default function createMongoRepository ({test}) {
  const dbHost = test
    ? process.env.CUSTOMCONNSTR_MONGO_TEST || 'mongodb://localhost/patientlife-test'
    : process.env.CUSTOMCONNSTR_MONGO || 'mongodb://localhost/patientlife';

  mongoose.connect(dbHost);
  return {
    userRepository,
    roleRepository,
    hospitalRepository,
    healthfileRepository,
    activityLogRepository,
    patientRecordRepository,
    approvalRequestRepository,
    noteRepository
  };
}