import ApprovalRequest from './models/ApprovalRequest';
import { processModel, removeProps, removeArrayProps } from './modelUtil';
import { compose, omit, map } from 'ramda';

const requesterPath = { path: 'requester', select: 'firstName lastName employeeId' };
const patientRecordPath = { path: 'patientRecord', select: '_id ssn diagnosis' };

const mapApprovalRequest = compose(
   x => ({
     ...x,
     requester: removeProps(x.requester),
     patientRecord: removeProps(x.patientRecord)
   }),
  processModel
);

async function getApprovalRequest (id) {
  const approvalRequest = await ApprovalRequest.findById(id)
    .populate(requesterPath)
    .populate(patientRecordPath);
  if (!approvalRequest) return null;
  return processModel(approvalRequest);
}

async function getApprovalRequestsFor (user) {
  const approvalRequests = await ApprovalRequest.find().where('hospital').equals(user.hospital.id)
    .populate(requesterPath)
    .populate(patientRecordPath)
    .sort({ requestedAt: 'desc' });
  if (!approvalRequests) return null;
  return map(mapApprovalRequest, approvalRequests);
}

async function pendingRequestExists (id) {
  const approvalRequest = await ApprovalRequest.findOne({ patientRecord: id }).where('status').equals('pending');
  if (!approvalRequest) return false;
  return true;
}

async function createApprovalRequest (approvalrequest) {
  const approvalRequestModel = new ApprovalRequest(approvalrequest);
  await approvalRequestModel.save();
  await ApprovalRequest.populate(approvalRequestModel, [requesterPath, patientRecordPath]);
  return mapApprovalRequest(approvalRequestModel);
}

async function updateApprovalRequest (id, approvalrequest) {
  const newApprovalRequest = await ApprovalRequest.findByIdAndUpdate(
    id,
    approvalrequest,
    {new: true}
  )
  .populate(requesterPath)
  .populate(patientRecordPath);

  return mapApprovalRequest(newApprovalRequest);
}

export default {
  getApprovalRequest,
  getApprovalRequestsFor,
  createApprovalRequest,
  updateApprovalRequest,
  pendingRequestExists
};