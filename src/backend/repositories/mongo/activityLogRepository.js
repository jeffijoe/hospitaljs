import ActivityLog from './models/ActivityLog';
import { processModel } from './modelUtil';
import { compose, omit, map, curry } from 'ramda';

/**
 * Gets all the activitylogs for the health file.
 * @param  {String} ID The search term.
 * @return {Object} The health files that was found.
 */
async function getActivityLogsByHealthFile (id) {
  const activitylogs = await ActivityLog.find({ 'healthfile.id': id });
  if (!activitylogs) return null;
  return map(processModel, activitylogs);
}

/**
 * Creates new activity log.
 * @param  {Object} That is the activitylog object. */
async function createActivityLog (activitylog) {
  const activityLogModel = new ActivityLog(activitylog);
  await activityLogModel.save();
  //return mapActivityLog(activityLogModel);
}

export default {
  getActivityLogsByHealthFile,
  createActivityLog
};