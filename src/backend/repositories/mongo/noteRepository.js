import Note from './models/Note';
import { processModel, removeProps, removeArrayProps } from './modelUtil';
import { compose, omit, map } from 'ramda';

const mapNotes = compose(
  x => ({
    ...x,
    user: removeProps(x.user),
    patientRecord: removeProps(x.patientRecord)
  }),
  processModel
);

async function getNotes (id) {
  const notes = await Note.find({'patientRecord': id})
    .populate({ path: 'user', select: 'firstName lastName' })
    .populate({ path: 'patientRecord', select: 'ssn' });
  if (!notes) return null;
  return map(processModel, notes);
}

async function createNote (note) {
  const noteModel = new Note(note);
  await noteModel.save();
  return processModel(noteModel);
}

export default {
  getNotes,
  createNote
};