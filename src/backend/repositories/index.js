/**
 * Creates repositories with the given provider.
 */
export default function createRepositories(provider, opts) {
  return require(`./${provider}`).default(opts);
}