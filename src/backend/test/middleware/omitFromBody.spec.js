import omitFromBody from '../../middleware/omitFromBody';
import {expect} from 'chai';
import {spy} from 'sinon';

describe('omit middleware', function() {
  it('should remove arguments from object', async function () {
    let ctx = {
      request: {
        body: {
          medication: '2 pills',
          symptoms: 'swollen arm',
          diagnosis: 'broken arm',
          dateStart: new Date(2016, 1, 22),
          dateModified: new Date(2016, 1, 22),
          labResults: 'x-ray'
        }
      }
    };
    const afterOmit = {
      medication: '2 pills',
      symptoms: 'swollen arm',
      diagnosis: 'broken arm',
      labResults: 'x-ray'
    };

    const next = spy(async () => true);
    await omitFromBody('dateStart', 'dateModified')(ctx, next);
    expect(next).to.have.been.calledOnce;
    expect(ctx.request.body).to.deep.equal(afterOmit);
  });
});