import permissions, { hasPermission } from '../../util/permissions';
import {expect} from 'chai';
import {spy} from 'sinon';
import authenticate from '../../middleware/authenticate';

describe('authenticate middleware', function() {
  it('lets us pass when required permissions = none, and we are authenticated', async function() {
    const user = {
      role: {
        permissions: permissions.none
      }
    };
    const ctx = {
      isAuthenticated: spy(x => true),
      req: {
        user
      }
    };

    const next = spy(async () => true);
    await authenticate(permissions.none)(ctx, next);
    expect(next).to.have.been.calledOnce;
    expect(ctx.user).to.equal(user);
  });

  it('returns unauthenticated when we are not authenticated', async function() {
    const ctx = {
      isAuthenticated: spy(x => false),
      unauthenticated: spy()
    };
    const next = spy(async () => true);
    await authenticate(permissions.none)(ctx, next);
    expect(ctx.unauthenticated).to.have.been.calledOnce;
    expect(next).to.not.have.been.called;
  });

  it(`returns unauthorized when we don't have the right permission`, async function() {
    const user = {
      role: {
        permissions: permissions.readHealthfile
      }
    };
    const ctx = {
      isAuthenticated: spy(x => true),
      unauthorized: spy(),
      req: {
        user
      }
    };
    const next = spy(async () => true);
    await authenticate(permissions.writeHealthfile)(ctx, next);
    expect(ctx.unauthorized).to.have.been.calledOnce;
    expect(next).to.not.have.been.called;
  });
});