import permissions, { hasPermission } from '../../util/permissions';
import {expect} from 'chai';

describe('permissions', function() {
  describe('hasPermission', function() {
    it('checks the permissions correctly', function() {
      expect(hasPermission(permissions.readHealthfile, permissions.writeHealthfile)).to.be.true;
      expect(hasPermission(permissions.readHealthfile, permissions.readHealthfile)).to.be.true;
      expect(hasPermission(permissions.writeHealthfile, permissions.readHealthfile)).to.be.false;
    });
  });
});