import {hash, compare} from '../../util/bcrypt';
import {expect} from 'chai';

describe('bcrypt', function() {
  describe('hash', function() {
    it('hashes the given input and returns a Promise', async function() {
      const hashed = await hash('bacon');
      expect(hashed).to.be.ok;
    });
  });

  describe('compare', function() {
    it('successfully compares hashes', async function() {
      const hashed = '$2a$10$V.iavP1.Qv2o2hr1mT.Gv.lbcXVT31i5XyJhkJb0IG/IdxEVFLa2m';
      const result = await compare('bacon', hashed);
      expect(result).to.be.true;
    });
  });
});