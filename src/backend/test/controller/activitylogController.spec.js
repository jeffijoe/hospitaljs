import { expect } from 'chai';
import supertest from 'supertest';
import { before, beforeEach, after } from 'mocha';
import createServer from '../../server.js';

describe('activityLogController test', async function() {
  let request, listener, healthfileId;

  before(async function (done) {
    this.timeout(10000);
    console.time('beforeactivitylog');
    const server = await createServer({ test: true }, done);
    request = supertest.agent(listener = server.listen());
    request
      .post('/login')
      .send({ employeeId: 'dk123', password: 'test' })
      .set('Accept', 'application/json;charset=utf8')
      .expect(200)
      .end(function (err, res) {
        console.log('err', err);

        if (err) return done(err);
        expect(res.headers).to.have.property('set-cookie');
        done();
      });
    console.timeEnd('beforeactivitylog');
  });

  after(function (done) {
    listener.close();
    done();
  });

  beforeEach(function (done) {
    this.timeout(10000);
    console.time('beforeEachactivitylog');
    request
      .get('/api/healthfiles/DK0102902894')
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        healthfileId = res.body.id;
        done();
      });
    console.timeEnd('beforeEachactivitylog');
  });

  it('should get activitylog for healthfile', function(done) {
    this.timeout(10000);
    console.time('activitylog');
    request
      .get('/api/healthfiles/' + healthfileId + '/activitylog/')
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
    console.timeEnd('activitylog');
  });
});