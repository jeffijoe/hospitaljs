import { expect } from 'chai';
import supertest from 'supertest';
import { before, beforeEach, after } from 'mocha';
import createServer from '../../server.js';

describe('notesController', function() {
  let request, listener, patientRecordId;

  before(async function (done) {
    this.timeout(10000);
    console.time('beforenotes');
    const server = await createServer({ test: true });
    request = supertest.agent(listener = server.listen());
    request
      .post('/login')
      .send({ employeeId: 'dk123', password: 'test' })
      .set('Accept', 'application/json;charset=utf8')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.headers).to.have.property('set-cookie');
        done();
      });
    console.timeEnd('beforenotes');
  });

  after(function (done) {
    listener.close();
    done();
  });

  beforeEach( async function (done) {
    this.timeout(5000);
    request
      .get('/api/approvalrequests')
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        patientRecordId = res.body[0].id;
        done();
      });
  });

  it('should get notes for patientrecord', async function (done) {
    this.timeout(5000);
    request
      .get('/api/patientrecords/' + patientRecordId + '/notes')
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('should post a note', async function (done) {
    this.timeout(5000);
    request
      .post('/api/patientrecords/' + patientRecordId + '/notes')
      .send({ message: 'patient very aggressive'})
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });
});