import { expect } from 'chai';
import supertest from 'supertest';
import { before, beforeEach, after } from 'mocha';
import createServer from '../../server.js';

describe('approvalRequestController', function() {
  let request, listener, patientRecordId;

  before(async function (done) {
    this.timeout(10000);
    console.time('beforeapprovalRequest');
    const server = await createServer({ test: true }, done);
    request = supertest.agent(listener = server.listen());
    request
      .post('/login')
      .send({ employeeId: 'dk123', password: 'test' })
      .set('Accept', 'application/json;charset=utf8')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.headers).to.have.property('set-cookie');
        done();
      });
    console.timeEnd('beforeapprovalRequest');
  });

  after(function (done) {
    listener.close();
    done();
  });

  it('should get approval requests', async function (done) {
    this.timeout(5000);
    request
      .get('/api/approvalrequests')
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        patientRecordId = res.body[0].id;
        done();
      });
  });

  it('should get one approval request', async function (done) {
    this.timeout(5000);
    request
      .get('/api/approvalrequests/' + patientRecordId)
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('should put approval request', async function (done) {
    this.timeout(5000);
    request
      .put('/api/approvalrequests/' + patientRecordId)
      .send({ status: 'rejected', rejectedReason: 'hm' })
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });
});