import { expect } from 'chai';
import supertest from 'supertest';
import { before, afterEach, after } from 'mocha';
import createServer from '../../server.js';

describe('healthfileController', function() {
  let request, listener;

  before(async function (done) {
    this.timeout(10000);
    console.time('beforehealthfile');
    const server = await createServer({ test: true });
    request = supertest.agent(listener = server.listen());
    request
      .post('/login')
      .send({ employeeId: 'dk123', password: 'test' })
      .set('Accept', 'application/json;charset=utf8')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.headers).to.have.property('set-cookie');
        done();
      });
    console.timeEnd('beforehealthfile');
  });

  after(function (done) {
    listener.close();
    done();
  });

  it('should get a single healthfile', function(done) {
    this.timeout(10000);
    request
      .get('/api/healthfiles/DK0102902894')
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('should post healthfile', function(done) {
    this.timeout(5000);
    request
      .post('/api/healthfiles/DK0102875421')
      .send({
        allergies: [{ name: 'Asthma', stage: 4, effect: 'Soft coughing' }],
        medication: [{ name: 'Sugar', instructions: 'Goes on oatmeal' }]
      })
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(function (res) {
        expect(res.body).to.have.property('ssn');
        expect(res.body).to.have.property('ownerHospital');
        expect(res.body).to.have.property('allergies');
        expect(res.body).to.have.property('medication');
        expect(res.body).to.have.property('id');
      })
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('should put healthfile', function(done) {
    this.timeout(5000);
    request
      .put('/api/healthfiles/DK0102875421')
      .send({
        allergies: [{ name: 'Corn', stage: 2, effect: 'Racer stomach' }],
        medication: [{ name: 'Sugar', instructions: 'Goes on oatmeal' }]
      })
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(function (res) {
        expect(res.body).to.have.property('ssn');
        expect(res.body).to.have.property('ownerHospital');
        expect(res.body).to.have.deep.property('ownerHospital.name');
        expect(res.body).to.have.deep.property('ownerHospital.location');
        expect(res.body).to.have.deep.property('ownerHospital.location.street');
        expect(res.body).to.have.deep.property('ownerHospital.location.city');
        expect(res.body).to.have.deep.property('ownerHospital.location.zip');
        expect(res.body).to.have.deep.property('ownerHospital.location.country');
        expect(res.body).to.have.property('allergies');
        expect(res.body).to.have.deep.property('allergies[0].name');
        expect(res.body).to.have.deep.property('allergies[0].stage');
        expect(res.body).to.have.deep.property('allergies[0].effect');
        expect(res.body).to.have.property('medication');
        expect(res.body).to.have.deep.property('medication[0].name');
        expect(res.body).to.have.deep.property('medication[0].instructions');
        expect(res.body).to.have.property('id');
      })
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });
});