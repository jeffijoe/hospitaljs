import { expect } from 'chai';
import supertest from 'supertest';
import { before, beforeEach, after } from 'mocha';
import createServer from '../../server.js';

describe('patientRecordController', function() {
  let request, listener, healthfileId, patientRecordId;

  before(async function (done) {
    this.timeout(10000);
    console.time('beforepatientRecord');
    const server = await createServer({ test: true });
    request = supertest.agent(listener = server.listen());
    request
      .post('/login')
      .send({ employeeId: 'dk123', password: 'test' })
      .set('Accept', 'application/json;charset=utf8')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.headers).to.have.property('set-cookie');
        done();
      });
    console.timeEnd('beforepatientRecord');
  });

  after(function (done) {
    listener.close();
    done();
  });

  beforeEach(function (done) {
    this.timeout(5000);
    request
      .get('/api/healthfiles/DK0102902894')
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        healthfileId = res.body.id;
        done();
      });
  });

  it('should get all patient records', async function(done) {
    this.timeout(5000);
    request
      .get('/api/healthfiles/' + healthfileId + '/patientrecords')
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        patientRecordId = res.body[0].id;
        done();
      });
  });

  it('should get a patient record', async function (done) {
    this.timeout(5000);
    request
      .get('/api/patientrecords/' + patientRecordId)
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('should post a patient record', function(done) {
    this.timeout(5000);
    request
      .post('/api/healthfiles/' + healthfileId + '/patientrecords')
      .send({
        medication: '2 pain killers', symptoms: 'swollen arm', diagnosis: '2 broken fingers'
      })
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        patientRecordId = res.body.id;
        done();
      });
  });

  it('should put a patient record', function(done) {
    this.timeout(5000);
    request
      .put('/api/patientrecords/' + patientRecordId)
      .send({
        medication: '2 pain killers', symptoms: 'swollen arm', diagnosis: '2 broken bones'
      })
      .set('Accept', 'application/json;charset=utf8')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });
});