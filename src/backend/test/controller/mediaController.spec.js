import { expect } from 'chai';
import supertest from 'supertest';
import { before, afterEach, after } from 'mocha';
import createServer from '../../server.js';

describe('mediaController test', function() {
  let request, listener;

  before(async function (done) {
    this.timeout(10000);
    console.time('beforemedia');
    const server = await createServer({ test: true });
    request = supertest.agent(listener = server.listen());
    request
      .post('/login')
      .send({ employeeId: 'dk123', password: 'test' })
      .set('Accept', 'application/json;charset=utf8')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.headers).to.have.property('set-cookie');
        done();
      });
    console.timeEnd('beforemedia');
  });

  after(function (done) {
    listener.close();
    done();
  });

  it('should get azure SAS', async function(done) {
    this.timeout(5000);
    request
      .get('/api/users/me')
      .set('Accept', 'application/json;charset=utf8')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });
});