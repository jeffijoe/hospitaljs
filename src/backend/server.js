/**
 * Dependencies.
 */
import Koa from 'koa';
import session from 'koa-session';
import convert from 'koa-convert';
import passport from 'koa-passport';
import responseTime from './middleware/responseTime';
import responseCalls from './middleware/responseCalls';
import chalk from 'chalk';
import bodyParser from 'koa-bodyparser';
import KoaRouter from 'koa-router';
import loadApi from './infrastructure/loadApi';
import createContainer from './infrastructure/container';
import createSeeder from './infrastructure/seed';
import env from './env';
import configureAuth from './infrastructure/auth';
import configureViewEngine from './infrastructure/viewEngine';
import serve from 'koa-static';
import path from 'path';
import webpackDevServer from './infrastructure/webpackDevServer';

/**
 * Creates a new server with the given options.
 * @param  {Boolean} opts.test Are we in test mode or not?
 * @return {KoaApplication}      The configured Koa app.
 */
let app;
let isOpen = false;
export default async function createServer (opts) {
  if (isOpen) return app;
  isOpen = true;
  // Creates a container.
  const container = createContainer(opts);

  // Port that the server listens on.
  // Azure sets this on the environment as PORT.
  const port = process.env.PORT || 9001;

  // Create a Koa application.
  app = new Koa();

  // Session keys.
  app.keys = ['HURRRDURR.js'];

  // Create a router
  const router = new KoaRouter();

  // Configure the view engine.
  configureViewEngine(app);

  // Configures Passport authentication.
  configureAuth(container, passport, router);

  // Set up middleware.
  app.use(bodyParser());
  app.use(convert(session({}, app)));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(responseTime);
  app.use(responseCalls);
  app.use(router.routes());
  app.use(router.allowedMethods());

  if (env.dev) {
    // In development, we don't need static files because Webpack Dev Middleware
    // serves it for us from memory - how neat!
    webpackDevServer(app);
  } else {
    // In production, this is where the files will be placed.
    app.use(convert(serve(path.join(__dirname, 'output'))));
  }

  // Load the API controllers.
  await loadApi(router, container);

  // Seed the database.
  await createSeeder(container)();

  // When other middleware isn't catching a request,
  // we just send back the app view.
  app.use(async function(ctx, next) {
    if (ctx.status !== 404) {
      await next();
      return;
    }

    const currentUser = ctx.req.user;
    ctx.render('app', { currentUser });
    await next();
  });

  // Start the server.
  app.listen(port, (err) => {
    if (err) throw err;
    console.log(chalk.cyan(`[Debug]: Hospital.js Server listening on port`), chalk.magenta.bold(port));
  });

  return app;
}