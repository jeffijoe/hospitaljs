require('babel-polyfill');
require('babel-register');
const Mocha = require('mocha');
const fs = require('fs');
const path = require('path');
const glob = require('glob');

const testDir = path.join(__dirname, '../test');
const testDirGlob = `${testDir}/**/*.js`;
const mocha = new Mocha({
  reporter: 'nyan'
});
mocha.addFile(path.join(testDir, 'index.js'));
glob(testDirGlob, (err, result) => {
  const files = result.filter(x => x.indexOf('backend/test/index.js') === -1);
  files.forEach(x => mocha.addFile(x));
  mocha.run();
});