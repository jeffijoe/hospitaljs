const eslint = require('eslint');

const CLIEngine = eslint.CLIEngine;

const cli = new CLIEngine();

const report = CLIEngine.getErrorResults(
  cli.executeOnFiles(['./']).results
);

const formatter = CLIEngine.getFormatter();
console.log(formatter(report));