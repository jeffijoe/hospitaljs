import permissions, {hasPermission} from '../util/permissions';
import { omit } from 'ramda';

export default function omitFromBody (...args) {
  return function (ctx, next) {
    ctx.request.body = omit(args, ctx.request.body);
    return next();
  };
}