import chalk from 'chalk';
import env from '../env';


/**
 * Logs response times for requests and sets it in the header.
 */
export default async function responseTime(ctx, next) {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', ms + 'ms');
  if (env.dev) {
    console.log(chalk.magenta(`[Timing] Request took ${ms}ms`));
  }
}