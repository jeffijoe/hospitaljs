import permissions, {hasPermission} from '../util/permissions';

export default function authenticate (requiredPermissions = permissions.none) {
  return async function (ctx, next) {
    const authenticated = ctx.isAuthenticated();
    if (!authenticated) {
      return ctx.unauthenticated();
    }

    const user = ctx.req.user;
    if (requiredPermissions !== permissions.none) {
      if (!hasPermission(requiredPermissions, user.role.permissions)) {
        return ctx.unauthorized(requiredPermissions);
      }
    }

    ctx.user = user;
    await next();
  };
}