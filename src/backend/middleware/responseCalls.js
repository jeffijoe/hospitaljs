import {getFriendlyPermissionName} from '../util/permissions';

function badRequest(ctx, body) {
  ctx.status = 400;
  ctx.body = body;
}

function notFound(ctx) {
  ctx.status = 404;
  ctx.body = {
    message: `Whatever you were looking for.. we ain't got it m8.`
  };
}

function ok(ctx, body) {
  ctx.status = body ? 200 : 204;
  ctx.body = body;
}

function unauthenticated(ctx) {
  ctx.status = 401;
  ctx.body = {
    message: 'Please authenticate yourself.'
  };
}

function unauthorized (ctx, requiredPermission) {
  const friendlyPermission = getFriendlyPermissionName(requiredPermission);
  ctx.status = 403;
  ctx.body = {
    message: `You need the '${friendlyPermission}' permission to access this endpoint.`
  };
}

/**
 * Logs response times for requests and sets it in the header.
 */
export default async function responseCalls(ctx, next) {
  ctx.badRequest = badRequest.bind(null, ctx);
  ctx.notFound = notFound.bind(null, ctx);
  ctx.ok = ok.bind(null, ctx);
  ctx.unauthenticated = unauthenticated.bind(null, ctx);
  ctx.unauthorized = unauthorized.bind(null, ctx);
  await next();
}