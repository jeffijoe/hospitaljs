export async function log({activityLogRepository}, props) {
  const {
    type,
    user,
    healthfile
  } = props;

  await activityLogRepository.createActivityLog({
    type: type,
    user: { employeeId: user.employeeId, firstName: user.firstName, lastName: user.lastName },
    healthfile: { ssn: healthfile.ssn, id: healthfile.id }
  });
}

export default function createActivityLogger(container) {
  return {
    log: container.bind(log)
  };
}