export async function create({ approvalRequestRepository }, props) {
  const {
    user,
    patientRecord
  } = props;

  const approvalRequest = await approvalRequestRepository.pendingRequestExists(patientRecord.id);

  if (!approvalRequest) {
    const req = await approvalRequestRepository.createApprovalRequest({
      requestedAt: Date.now(),
      status: 'pending',
      requester: user.id,
      patientRecord: patientRecord.id,
      hospital: user.hospital.id
    });
  }
}

export default function createApprovalRequest (container) {
  return {
    create: container.bind(create)
  };
}