require('babel-polyfill');
require('babel-register');
require('source-map-support/register');
var env = require('./env').default;

if (env.dev) {
  require('./bin/lint');
  /*
  require('./bin/test');
  */
}

const createServer = require('./server').default;
createServer({});