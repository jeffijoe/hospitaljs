import createRepositories from '../repositories';
import createActivityLogger from '../services/activityLogger';
import createApprovalRequest from '../services/approvalRequestService';
import azure from 'azure-storage';
import config from '../config';


/**
 * Creates a new container.
 * The container contains the modules to use and are injected
 * into routes.
 */
export default function createContainer({ test }) {
  const container = {
    bind(fn) {
      return fn.bind(null, container);
    }
  };

  Object.assign(container, {
    ...createRepositories('mongo', { test }),
    blobService: azure.createBlobService(config.AZURE_STORAGE_ACCOUNT, config.AZURE_STORATE_ACCESS_KEY)
  });

  container.activityLogger = createActivityLogger(container);
  container.approvalRequestService = createApprovalRequest(container);
  return container;
}
