import passportLocal from 'passport-local';
import {last} from 'ramda';

const wrapErrors = fn => {
  return async function () {
    try {
      await fn(...arguments);
    } catch (e) {
      const args = Array.from(arguments);
      last(args)(e);
    }
  };
};

const LocalStrategy = passportLocal.Strategy;
export default function configureAuth(container, passport, router) {
  const { userRepository } = container;

  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser(wrapErrors(async (id, done) => {
    const user = await userRepository.getUserById(id);

    if (!user) {
      return done(null, false);
    }

    done(null, user);
  }));

  passport.use(new LocalStrategy(
    {usernameField: 'employeeId'},
    wrapErrors(async (username, password, done) => {

      const user = await userRepository.getUserByCredentials({
        employeeId: username,
        password
      });

      if (!user) {
        return done(null, false);
      }

      done(null, user);
    })));

  router.post(
    '/login',
    passport.authenticate('local'),
    (ctx) => {
      if (ctx.isAuthenticated()) {
        return ctx.ok(ctx.req.user);
      }

      ctx.status = 401;
      ctx.body = {
        message: 'Wrong credentials.'
      };
    }
  );

  router.post('/logout', ctx => {
    ctx.logout();
    ctx.ok();
  });
}
