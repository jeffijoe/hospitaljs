import mongoose from 'mongoose';
import permissions from '../util/permissions';

export default function createSeeder(container) {
  const {
    userRepository,
    roleRepository,
    hospitalRepository,
    healthfileRepository,
    activityLogRepository,
    patientRecordRepository,
    approvalRequestRepository,
    noteRepository
  } = container;

  const dropCollection = collectionName => {
    return new Promise((resolve, reject) => {
      mongoose.connection.collections[collectionName].drop(function() {
        return resolve();
      });
    });
  };

  const users = [{
    employeeId: 'dk123',
    firstName: 'Jeff',
    lastName: 'Hansen',
    department: 'Swagness',
    phoneNumber: '12345678',
    password: 'test',
    email: 'jeffijoe@pl.com',
    hospital: 0,
    role: 0
  }, {
    employeeId: 'dk124',
    firstName: 'Perry',
    lastName: 'Cox',
    department: 'Sissy-management',
    phoneNumber: '87654322',
    password: 'test',
    email: 'cox@pl.com',
    hospital: 0,
    role: 1
  }, {
    employeeId: 'dk125',
    firstName: 'Mads',
    lastName: 'Falkenstrøm',
    department: 'Swagger',
    phoneNumber: '87654321',
    password: 'hejsa',
    email: 'nobis@pl.com',
    hospital: 0,
    role: 2
  }, {
    employeeId: 'us123',
    firstName: 'Gregory',
    lastName: 'House',
    department: 'Brilliancy',
    phoneNumber: '555 123 123 4444',
    password: 'test',
    email: 'house@pl.com',
    hospital: 1,
    role: 1
  }];

  const roles = [{
    name: 'Grand master',
    permissions: permissions.readHealthfile
                 | permissions.writeHealthfile
                 | permissions.approveHealthfile
                 | permissions.shareHealthfile
  }, {
    name: 'Head Doctor',
    permissions: permissions.readHealthfile
                 | permissions.writeHealthfile
                 | permissions.approveHealthfile
  }, {
    name: 'Peasant',
    permissions: permissions.readHealthfile
  }];

  const hospitals = [{
    name: 'Sønderborg Hospital',
    location: {
      street: 'Sygehusgade 123',
      city: 'Sønderborg',
      zip: '6400',
      country: 'DK'
    }
  }, {
    name: 'Sacred Heart Hospital',
    location: {
      street: 'Murica Street 1969',
      city: 'New York',
      zip: '1234',
      country: 'US'
    }
  }];

  const healthfiles = [{
    ssn: 'DK0102902894',
    ownerHospital: 0,
    allergies: [{
      name: 'Asthma',
      stage: 4,
      effect: 'Intensive coughing'
    }, {
      name: 'Chronic awesomeness',
      stage: 5,
      effect: 'Constant outbursts of pure swag'
    }],
    medication: [{
      name: 'Singulair',
      instructions: 'Take once a day'
    }, {
      name: 'Pro-depressive pills',
      instructions: 'Pop one whenever you are feeling too good'
    }]
  }, {
    ssn: 'US721074426',
    ownerHospital: 1,
    allergies: [{
      name: 'Peanuts',
      stage: 4,
      effect: 'Swolling of throat causing asphyxiation'
    }],
    medication: []
  }];

  const patientrecords = [{
    medication: null,
    healthfileId: 0,
    symptoms: 'patient hit his head against the curb while skateboarding',
    diagnosis: 'concussion',
    bodyParts: [{
      bodyPart: 'path1513'
    }],
    procedures: [{
      step: 'bedrest, constant observation'
    }, {
      step: 'ice cream, best served cold'
    }]
  }, {
    healthfileId: 0,
    medication: '2 painkillers, 1 morphine',
    symptoms: 'patient fell down from swing, now his forearm is swollen and hurts when touched/moved',
    diagnosis: 'broken left forearm',
    labResults: 'X-ray of forearm',
    bodyParts: [{
      bodyPart: 'path1689'
    }],
    procedures: [{
      step: 'Applied cast'
    }],
    media: [{
      mediaType: 'photo',
      fileName: 'bonebreak.jpg',
      fileSize: '85mb',
      fileUrl: 'http://what-the-heck.com/wp-content/uploads/2015/07/bonebreak.jpg'
    }]
  }, {
    healthfileId: 0,
    symptoms: 'Itchy feet, swollen and red',
    diagnosis: 'Fungal infection of feet (tinea pedis)',
    medication: 'Prescribed pills, 1 per day for 28 days',
    bodyParts: [{
      'bodyPart': 'path4885'
    }, {
      'bodyPart': 'path3310'
    }, {
      'bodyPart': 'path4910'
    }, {
      'bodyPart': 'path3286'
    }, {
      'bodyPart': 'path3304'
    }],
    media: [{
      mediaType: 'photo',
      fileName: 'foot.jpg',
      fileSize: '2mb',
      fileUrl: 'http://i.imgur.com/zU53ilZ.jpg'
    }]
  }];

  const approvalrequests = [
    {
      requestedAt: new Date(2016, 0, 25, 14, 11),
      patientRecord: 0,
      requester: 1,
      hospital: 0
    },
    {
      requestedAt: new Date(2015, 10, 29, 11, 16),
      status: 'rejected',
      rejectedReason: 'X-ray shows right forearm, please have a look at this',
      patientRecord: 1,
      requester: 0,
      hospital: 0
    },
    {
      requestedAt: new Date(2016, 0, 1, 14, 54),
      status: 'approved',
      patientRecord: 0,
      requester: 0,
      hospital: 0
    },
    {
      requestedAt: new Date(2016, 0, 1, 15, 19),
      status: 'approved',
      patientRecord: 2,
      requester: 1,
      hospital: 0
    }
  ];

  return async function seed() {
    try {
      const collections = ['users', 'roles', 'hospitals', 'healthfiles', 'activitylogs', 'patientrecords', 'approvalrequests', 'notes'];
      await Promise.all(collections.map(dropCollection));
      const newHospitals = await Promise.all(hospitals.map(x => hospitalRepository.createHospital(x)));
      const newRoles = await Promise.all(roles.map(x => roleRepository.createRole(x)));
      let roleCounter = 0;
      const newUsers = await Promise.all(users.map(x => userRepository.createUser({
        ...x,
        role: newRoles[x.role].id,
        hospital: newHospitals[x.hospital].id
      })));
      const newHealthFiles = await Promise.all(healthfiles.map(x => healthfileRepository.createHealthfile({
        ...x,
        ownerHospital: newHospitals[x.ownerHospital].id
      })));
      const newPatientRecords = await Promise.all(
        patientrecords.map(
          x => patientRecordRepository.createPatientRecord(newHealthFiles[x.healthfileId].id, x)
        )
      );
      const newApprovalRequests = await Promise.all(approvalrequests.map(x => approvalRequestRepository.createApprovalRequest({
        ...x,
        requester: newUsers[x.requester].id,
        patientRecord: newPatientRecords[x.patientRecord].id,
        hospital: newHospitals[x.hospital].id
      })));

    } catch (e) {
      console.error('error while seeding: ', e);
    }
  };
}
