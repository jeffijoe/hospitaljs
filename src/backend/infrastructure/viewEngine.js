import KoaJade from 'koa-jade';
import path from 'path';
import env from '../env';

export default function configureViewEngine (app) {
  const viewPath = path.join(__dirname, '../views');
  return new KoaJade({
    viewPath,
    basedir: viewPath,
    debug: env.dev,
    pretty: env.dev,
    compileDebug: env.dev,
    app
  });
}