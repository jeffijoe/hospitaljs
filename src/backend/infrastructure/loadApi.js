import fs from 'fs';
import path from 'path';

const apiPath = path.join(__dirname, '../api');

/**
 * Scans the `api` folder and imports and invokes
 * it with the container and router.
 * @param  {KoaRouter} router    The router.
 * @param  {Object} container The container.
 * @return {Promise}          A promise that resolves when we're done.
 */
export default function loadApi(router, container) {
  return new Promise((resolve, reject) => {
    console.log('API path', apiPath);
    fs.readdir(apiPath, (err, results) => {
      if (err) return reject(err);
      results.map(x => require(path.join(apiPath, x)).default).forEach(x => x(router, container));
      resolve();
    });
  });
}