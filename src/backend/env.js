const env = process.env.NODE_ENV;
const prod = env === 'production';

/**
 * Tells us what environment we are in.
 */
export default {
  dev: !prod,
  prod
};