import bcrypt from 'bcrypt-nodejs';

/**
 * Hashes the input string.
 * @param  {String} str The string to hash.
 * @return {Promise<String>}     The hashed string.
 */
export function hash(str) {
  return new Promise((resolve, reject) => {
    bcrypt.hash(str, null, null, (err, result) => {
      if (err) return reject(err);
      return resolve(result);
    });
  });
}

/**
 * Compares an unhashed string with a hashed one.
 * @param  {String} unhashed The unhashed string.
 * @param  {String} hashed   The hashed string.
 * @return {Promise<Boolean>}          Flag indicating whether the given input are equal.
 */
export function compare(unhashed, hashed) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(unhashed, hashed, (err, result) => {
      if (err) return reject(err);
      return resolve(result);
    });
  });
}