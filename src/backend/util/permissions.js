import {memoize} from 'ramda';

const permissions = {
  none:           0b00000000,
  readHealthfile:    0b00000001,
  writeHealthfile:   0b00000011,
  approveHealthfile: 0b00000111,
  shareHealthfile:   0b00001001,
  all:            0b11111111
};

export default permissions;
export const permissionKeys = Object.keys(permissions);

export const getFriendlyPermissionName = memoize(function getFriendlyPermissionName(permission) {
  return permissionKeys.find(key => permissions[key] === permission);
});

export const hasPermission = memoize(function hasPermission(requiredPermission, currentPermission) {
  return (currentPermission & requiredPermission) >= requiredPermission;
});