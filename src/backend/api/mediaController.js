import authenticate from '../middleware/authenticate';
import permissions from '../util/permissions';
import config from '../config';
import azure from 'azure-storage';
import { concat } from 'ramda';
import moment from 'moment';
import {memoize} from 'ramda';

const createContainerIfNotExists = memoize((blobService, hospitalId) => {
  return new Promise((resolve, reject) => {
    blobService.createContainerIfNotExists(hospitalId, {publicAccessLevel : 'blob'}, function(error, result, response) {
      if (error) return reject(error);
      return resolve(response);
    });
  });
});

async function getMediaToken ({blobService}, ctx) {
  await new Promise((resolve, reject) => {
    blobService.setServiceProperties(config.serviceProperties, function(error, result, response) {
      if (error) return reject(error);
      return resolve();
    });
  });

  const hospital = ctx.user.hospital.id.toString();
  let startDate = moment().add(-1, 'days').toDate();
  let expiryDate = moment().add(+1, 'days').toDate();
  await createContainerIfNotExists(blobService, hospital);
  const token = blobService.generateSharedAccessSignature(hospital, null, config.sharedAccessPolicy(startDate, expiryDate));
  // const token = await new Promise((resolve, reject) => {
  //   blobService.createContainerIfNotExists(hospital, {publicAccessLevel : 'blob'}, function(error, result, response){
  //     if (error) return reject(error);

  //     return resolve(t);
  //   });
  // });
  ctx.ok({ token });
}

export default function (router, { bind }) {
  router
    .get(
      '/api/mediatoken',
      authenticate(permissions.writeHealthfile),
      bind(getMediaToken)
    );
}