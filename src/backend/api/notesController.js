import authenticate from '../middleware/authenticate';
import omitFromBody from '../middleware/omitFromBody';
import permissions from '../util/permissions';
import {merge} from 'ramda';

async function getNotes ({ noteRepository }, ctx) {
  const notes = await noteRepository.getNotes(ctx.params.id);
  if (!notes) return ctx.notFound();
  return ctx.ok(notes);
}

async function createNote ({ noteRepository }, ctx) {
  //const note = merge(ctx.request.body, { user: ctx.user.id, patientRecord: ctx.params.id });
  const note = {
    ...ctx.request.body,
    user: ctx.user.id,
    patientRecord: ctx.params.id
  };
  const newNote = await noteRepository.createNote(note);
  return ctx.ok(newNote);
}

export default function (router, { bind }) {
  router
    .get(
      '/api/patientrecords/:id/notes',
      authenticate(permissions.readHealthfile),
      bind(getNotes)
    )
    .post(
      '/api/patientrecords/:id/notes',
      authenticate(permissions.writeHealthfile),
      omitFromBody('createdAt', 'user'),
      bind(createNote)
    );
}