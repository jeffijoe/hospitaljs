import authenticate from '../middleware/authenticate';
import omitFromBody from '../middleware/omitFromBody';
import permissions from '../util/permissions';
import { merge } from 'ramda';

async function getPatientRecord ({ patientRecordRepository }, ctx) {
  const patientRecord = await patientRecordRepository.getPatientRecord(ctx.params.id);
  if (!patientRecord) return ctx.notFound();
  return ctx.ok(patientRecord);
}

async function getPatientRecords ({ patientRecordRepository }, ctx) {
  const patientRecords = await patientRecordRepository.getPatientRecords(ctx.params.healthfileId);
  if (!patientRecords) return ctx.notFound();
  return ctx.ok(patientRecords);
}

async function createPatientRecord ({ patientRecordRepository, approvalRequestService }, ctx) {
  const patientRecord = await patientRecordRepository.createPatientRecord(ctx.params.healthfileId, ctx.request.body);
  approvalRequestService.create({ user: ctx.user, patientRecord });
  return ctx.ok(patientRecord);
}

async function updatePatientRecord ({ patientRecordRepository, approvalRequestService }, ctx) {
  const requestBody = merge(ctx.request.body, {dateModified: Date.now()});
  const patientRecord = await patientRecordRepository.updatePatientRecord(ctx.params.id, requestBody);
  if (!patientRecord) return ctx.notFound();
  approvalRequestService.create({ user: ctx.user, patientRecord });
  return ctx.ok(patientRecord);
}

export default function (router, { bind }) {
  router
    .get(
      '/api/patientrecords/:id',
      authenticate(permissions.readHealthfile),
      bind(getPatientRecord)
    )
    .get(
      '/api/healthfiles/:healthfileId/patientrecords',
      authenticate(permissions.readHealthfile),
      bind(getPatientRecords)
    )
    .post(
      '/api/healthfiles/:healthfileId/patientrecords',
      authenticate(permissions.writeHealthfile),
      omitFromBody('dateStart', 'dateModified', 'approvalRequests'),
      bind(createPatientRecord)
    )
    .put(
      '/api/patientrecords/:id',
      authenticate(permissions.writeHealthfile),
      omitFromBody('dateStart', 'approvalRequests', 'dateModified', 'healthfileId'),
      bind(updatePatientRecord)
    );
}