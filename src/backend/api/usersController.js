import authenticate from '../middleware/authenticate';

function getCurrentUser ({}, ctx) {
  ctx.ok(ctx.user);
}

export default function (router, {bind}) {
  router.get('/api/users/me',
    authenticate(),
    bind(getCurrentUser)
  );
}