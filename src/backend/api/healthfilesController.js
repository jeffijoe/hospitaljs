import cprregisterRepository from '../repositories/external/cprregisterRepository';
import authenticate from '../middleware/authenticate';
import permissions from '../util/permissions';

import { merge, set, lensProp } from 'ramda';

async function getHealthfile ({ healthfileRepository, activityLogger }, ctx) {
  let ssn = ctx.params.ssn;
  const healthfile = await healthfileRepository.getHealthfile(ssn);

  if (healthfile) {
    ssn = healthfile.ssn;
    activityLogger.log({ type: 'read', user: ctx.user, healthfile: healthfile });
  }

  const civilian = await cprregisterRepository.getCivilian(ssn);
  if (!civilian) return ctx.notFound();
  const hf = merge(healthfile, civilian);
  return ctx.ok(hf);
}

async function createHealthfile ({healthfileRepository, activityLogger}, ctx) {
  const ownerHospital = ctx.user.hospital.id;
  const newhealthfile = merge({ ssn: ctx.params.ssn }, {ownerHospital});
  const healthfile = await healthfileRepository.createHealthfile(newhealthfile);
  activityLogger.log({ type: 'create', user: ctx.user, healthfile: healthfile });
  return ctx.ok(healthfile);
}

//from utils.js
//const updateObject = (field, newProp, object) => set(lensProp(field), newProp, object);

async function updateHealthfile ({healthfileRepository, activityLogger}, ctx) {
  // const requestBody = set(lensProp('ownerHospital'), ctx.user.hospital.id, ctx.request.body);
  // const requestBody = updateObject('ownerHospital', ctx.user.hospital.id, ctx.request.body);
  const requestBody = {
    ...ctx.request.body,
    ownerHospital: ctx.user.hospital.id
  };
  const healthfile = await healthfileRepository.updateHealthfile(ctx.params.ssn, requestBody);
  if (!healthfile) return ctx.notFound();
  activityLogger.log({ type: 'update', user: ctx.user, healthfile: healthfile });
  return ctx.ok(healthfile);
}

export default function (router, { bind }) {
  router
    .get(
      '/api/healthfiles/:ssn',
      authenticate(permissions.readHealthfile),
      bind(getHealthfile)
    )
    .post(
      '/api/healthfiles/:ssn',
      authenticate(permissions.writeHealthfile),
      bind(createHealthfile)
    )
    .put(
      '/api/healthfiles/:ssn',
      authenticate(permissions.writeHealthfile),
      bind(updateHealthfile)
    );
}