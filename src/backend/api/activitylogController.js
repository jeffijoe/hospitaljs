import authenticate from '../middleware/authenticate';
import permissions from '../util/permissions';

async function getActivityLog ({ activityLogRepository }, ctx) {
  const activityLogs = await activityLogRepository.getActivityLogsByHealthFile(ctx.params.healthfileId);
  if (!activityLogs) return ctx.notFound();
  return ctx.ok(activityLogs);
}

export default function (router, { bind }) {
  router
    .get(
      '/api/healthfiles/:healthfileId/activitylog/',
      authenticate(permissions.readHealthfile),
      bind(getActivityLog)
    );
}