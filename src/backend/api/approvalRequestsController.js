import authenticate from '../middleware/authenticate';
import omitFromBody from '../middleware/omitFromBody';
import permissions from '../util/permissions';

async function getApprovalRequest ({ approvalRequestRepository }, ctx) {
  const approvalRequest = await approvalRequestRepository.getApprovalRequest(ctx.params.id);
  if (!approvalRequest) return ctx.notFound();
  return ctx.ok(approvalRequest);
}

async function getApprovalRequests ({ approvalRequestRepository }, ctx) {
  const approvalRequests = await approvalRequestRepository.getApprovalRequestsFor(ctx.user);
  if (!approvalRequests) return ctx.notFound();
  return ctx.ok(approvalRequests);
}

/*async function createApprovalRequests ({ approvalRequestRepository }, ctx) {
  const approvalRequest = await approvalRequestRepository.createApprovalRequest(ctx.request.body);
  return ctx.ok(approvalRequest);
}*/

async function updateApprovalRequests ({ approvalRequestRepository }, ctx) {
  const approvalRequest = await approvalRequestRepository.updateApprovalRequest(ctx.params.id, ctx.request.body);
  if (!approvalRequest) return ctx.notFound();
  return ctx.ok(approvalRequest);
}

export default function (router, { bind }) {
  router
    .get(
      '/api/approvalrequests/:id',
      authenticate(permissions.approveHealthfile),
      bind(getApprovalRequest)
    )
    .get(
      '/api/approvalrequests',
      authenticate(permissions.approveHealthfile),
      bind(getApprovalRequests)
    )
    /*.post(
      '/api/approvalrequests',
      authenticate(permissions.approveHealthfile),
      omitFromBody('requestedAt', 'rejectedReason', 'status'),
      bind(createApprovalRequests)
    )*/
    .put(
      '/api/approvalrequests/:id',
      authenticate(permissions.approveHealthfile),
      omitFromBody('requestedAt', 'requester', 'patientRecord'),
      bind(updateApprovalRequests)
    );
}