import azure from 'azure-storage';

const AZURE_STORAGE_ACCOUNT = 'patientlife';
const AZURE_STORATE_ACCESS_KEY = 'eJ5VJ6Rx1y7MsE0xYlSSl8a5qBwOZvDIHzI/ZORnaLzT0g9KFcpV+/ZltSDRGCy25BJdFWRhy6nihWvr6yB7pQ==';
const AZURE_URL = 'https://patientlife.blob.core.windows.net/';
const serviceProperties = {
  Cors: {
    CorsRule: [{
      AllowedOrigins: ['*'],
      AllowedMethods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'HEAD', 'PATCH'],
      AllowedHeaders: ['*'],
      ExposedHeaders: ['*'],
      MaxAgeInSeconds: 600
    }]
  }
};

const sharedAccessPolicy = (startDate, expiryDate) => {
  return {
    AccessPolicy: {
      // Permissions: azure.BlobUtilities.SharedAccessPermissions.READ | azure.BlobUtilities.SharedAccessPermissions.WRITE,
      Permissions: 'rwdl',
      Start: startDate,
      Expiry: expiryDate
    }
  };
};

const config = {
  AZURE_STORAGE_ACCOUNT: AZURE_STORAGE_ACCOUNT,
  AZURE_STORATE_ACCESS_KEY: AZURE_STORATE_ACCESS_KEY,
  AZURE_URL: AZURE_URL,
  serviceProperties: serviceProperties,
  sharedAccessPolicy: sharedAccessPolicy
};

export default config;