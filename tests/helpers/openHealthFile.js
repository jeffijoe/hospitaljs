const doLogin = require('./doLogin');

module.exports = function (ctx, ssn) {
  return doLogin(ctx).then(() => {
    return new ctx.Widget({
      root: '#search'
    }).sendKeys(ssn);
  }).then(() => {
    return ctx.Widget.click('#open');
  });
};