module.exports = function (ctx) {
  return ctx.driver.get('http://localhost:9001/login').then(() => {
    return new ctx.Widget({
      root: '#username'
    }).sendKeys('dk123');
  }).then(() => {
    return new ctx.Widget({
      root: '#password'
    }).sendKeys('test');
  }).then(() => {
    return ctx.Widget.click('#login');
  }).then(() => {
    return ctx.driver.wait(function () {
      return ctx.driver.getCurrentUrl().then(function (url) {
        //url.should.eql('http://localhost:9001/');
        return true;
      });
    }, 2000);
  });
};