const doLogin = require('../helpers/doLogin');

module.exports = function(){

  this.Given(/^I'm logged in$/, function() {
    return doLogin(this);
  });

  this.When(/^I enter a patients SSN \"([^\"]*)\"$/, function(value){
    return new this.Widget({
      root: '#search'
    }).sendKeys(value);
  });

  this.When(/^I click on open/, function () {
    return this.Widget.click('#open');
  });

  this.Then(/^I should see the health file page$/, function() {
    return this.driver.getCurrentUrl().then(function (url) {
      return url.should.contain('http://localhost:9001/healthfile/');
    });
  });
};