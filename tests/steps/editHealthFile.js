const openHealthFile = require('../helpers/openHealthFile');

module.exports = function(){

  this.Given(/^I've found a health file$/, function() {
    return openHealthFile(this, 'DK0102902894');
  });

  this.When(/^I click on edit for health file$/, function() {
    return this.Widget.click('#editHealthfile');
  });

  this.When(/^I click on add for medication$/, function() {
    return this.Widget.click('#medication-editor *[data-pioneer="add item"]');
  });

  this.When(/^I enter name "([^"]*)"$/, function(value) {
    return new this.Widget({
      root: '#medication-editor *[data-pioneer="item editor"]:last-child input'
    }).sendKeys(value);
  });

  this.When(/^I enter instructions "([^"]*)"$/, function(value) {
    return new this.Widget({
      root: '#medication-editor *[data-pioneer="item editor"]:last-child div:nth-of-type(2) input'
    }).sendKeys(value);
  });

  this.When(/^I click on save$/, function() {
    return this.Widget.click('#saveHealthFile');
  });

  this.Then(/^I should see the updated health file$/, function() {
    return this.driver.getCurrentUrl().then(function (url) {
      //^[a-zA-Z0-9]{12} - to match letter, numbers and length of 12
      return url.should.contain('http://localhost:9001/healthfile/');
    });
  });

};