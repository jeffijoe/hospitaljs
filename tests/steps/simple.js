module.exports = function(){

  this.Given(/^I visit Hospital.js$/, function(){
    this.driver.get('http://localhost:9001/login');
  });

  this.When(/^I enter username \"([^\"]*)\"$/, function(value){
    return new this.Widget({
      root: '#username'
    }).sendKeys(value);
  });

  this.When(/^I enter password \"([^\"]*)\"$/, function(value){
    return new this.Widget({
      root: '#password'
    }).sendKeys(value);
  });

  this.When(/^I press the login button/, function () {
    return this.Widget.click('#login');
  });

  this.Then(/^I should see the dashboard/, function() {
    return this.driver.getCurrentUrl().then(function (url) {
      return url.should.eql('http://localhost:9001/');
    });
  });

};