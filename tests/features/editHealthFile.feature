Feature: Editing the health file

  Background:
    Given I've found a health file

  Scenario: Add a new medication
    When I click on edit for health file
    And I click on add for medication
    And I enter name "Thiazider"
    And I enter instructions "Take one with every meal - 3 times a day"
    And I click on save
    Then I should see the updated health file