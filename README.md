# HospitalJS

by Jeff Hansen & Mads Falkenstrøm


## Directory structure

The following is the application structure for the project.

* `src` - source code for the web application and API.
    - `backend` - the API module
    - `frontend` - the web app as a React + Redux application